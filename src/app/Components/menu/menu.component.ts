import {Component, Input} from '@angular/core';
import {Router} from '@angular/router'


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {
  @Input() titulo: any;
  nombres: string;
  matricula: string;
  permisos: string;
  tipoUsuario: string;
  paginas = [
    {pagina: 'Inicio', id: 0, ruta: 'home'},
    {pagina: 'Autores', id: 1, ruta: 'autores'},
    {pagina: 'Categorias', id: 2, ruta: 'categorias'},
    {pagina: 'Libro', id: 3, ruta: 'libros'},
    {pagina: 'Editoriales', id: 4, ruta: 'editoriales'},
    {pagina: 'Carreras', id: 5, ruta: 'carreras'},
    {pagina: 'Control de prestamos', id: 7, ruta: 'control-prestamos'},
    {pagina: 'Mi Administrador', id: 8, ruta: 'administrador'},
    {pagina: 'Prestamos', id: 10, ruta: 'prestamos'},
    {pagina: 'Solicitar Prestamo', id: 11, ruta: 'solicitar-prestamo'},
    {pagina: 'Mi Usuario', id: 12, ruta: 'alumno'},
    {pagina: 'Tipo de Usuarios', id: 13, ruta: 'tipo-usuarios'},
    {pagina: 'Nuevo Usuario', id: 14, ruta: 'nuevo-usuario'},
    {pagina: 'Mi Super Administrador', id: 15, ruta: 'super-administrador'}
  ]

  paginasPermitidas: Array<any> = [];

  constructor(readonly router: Router) {
    this.matricula = sessionStorage?.matricula;
    this.nombres = sessionStorage?.nombre;
    this.permisos = sessionStorage?.permisos;
    this.tipoUsuario = sessionStorage?.tipoUsuario;
    this.permisos.split(',')
      .map(id => parseInt(id, 10))
      .map(id => this.paginas.find(valor => valor.id === id))
      .forEach(valor => this.paginasPermitidas.push(valor));
  }


  go(path: string): void {
    this.router.navigateByUrl(path);
  }

  closeSession() {
    sessionStorage.clear()
    this.router.navigateByUrl('')
  }

}
