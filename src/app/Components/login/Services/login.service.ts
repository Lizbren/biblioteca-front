import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { HttpLink} from 'apollo-angular/http';
import { InMemoryCache} from '@apollo/client/core';
import { environment } from 'src/environments/environment';
import { LOGIN_USUARIO} from 'src/app/Querys/sesion';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private apollo: Apollo,
    private httpLink: HttpLink
  ) {
    apollo.create({
      link: httpLink.create({ uri: environment.BIBLIOTECA }),
      cache: new InMemoryCache()
    }, 'umbLogin');
   }

   loginUsuario(credenciales: any){
      return this.apollo.use('umbLogin').query({
        query: LOGIN_USUARIO,
        variables:{
          credenciales
        },
        fetchPolicy: 'no-cache'
      }).toPromise();
   }
}
