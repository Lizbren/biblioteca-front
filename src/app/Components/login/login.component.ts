import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {LoginService} from 'src/app/Components/login/Services/login.service';
import {AppComponent} from "../../app.component";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  loginForm: FormGroup;
  showProgress: boolean = false;
  failLogin = false;

  constructor(
    readonly router: Router,
    readonly loginService: LoginService,
    readonly app: AppComponent
  ) {
    this.loginForm = new FormGroup({
      matricula: new FormControl('', [
        Validators.required,
        Validators.maxLength(8),
        Validators.minLength(5),
        Validators.pattern('^[0-9]*')
      ]),
      password: new FormControl('', [
        Validators.required
      ])
    });

  }

  get matricula() {
    return this.loginForm.controls.matricula;
  }

  get password() {
    return this.loginForm.controls.password;
  }

  login(): void {
    this.showProgress = true;
    this.loginService.loginUsuario({matricula: this.matricula.value, clave: this.password.value})
      .then((usuario: any) => {
        const usuarioLogueado = usuario.data['loginUsuario'];
        sessionStorage.setItem('logged', 'true');
        sessionStorage.setItem('matricula', usuarioLogueado.matricula);
        sessionStorage.setItem('nombre', `${usuarioLogueado.nombres} ${usuarioLogueado.primerApellido} ${usuarioLogueado.segundoApellido}`);
        sessionStorage.setItem('tipoUsuario', usuarioLogueado.tipoUsuario);
        sessionStorage.setItem('idTipoUsuario', usuarioLogueado.idTipoUsuario);
        sessionStorage.setItem('permisos', usuarioLogueado.permisos);
        sessionStorage.setItem('correo', usuarioLogueado.correo);
        sessionStorage.setItem('idUsuario', usuarioLogueado.idUsuario);
        sessionStorage.setItem('carrera', usuarioLogueado.carrera);
        sessionStorage.setItem('fechaAlta', usuarioLogueado.fechaAlta);
        sessionStorage.setItem('cicloActual', usuarioLogueado.cicloActual);
        sessionStorage.setItem('semestre', usuarioLogueado.semestre);
        this.app.loadMenu();
        this.redirectHome();
      })
      .catch(() => {
        this.showProgress = false;
        this.failLogin = true;
        setTimeout(() => {
          this.failLogin = false;
        }, 5000)
      })

  }

  redirectHome() {
    this.router.navigateByUrl('home')
  }

}
