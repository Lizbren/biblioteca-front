import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './Components/login/login.component';

import {AuthService} from './authguards/logged-auth';
import {AnonymousService} from './authguards/anonymous-auth';


const routes: Routes = [
  {path: '', component: LoginComponent, canActivate: [AnonymousService]},
  {
    path: 'home',
    loadChildren: () => import('./Modules/home/home.module').then(m => m.HomeModule),
    canActivate: [AuthService]
  },
  {
    path: 'autores',
    loadChildren: () => import('./Modules/autores/autores.module').then(m => m.AutoresModule),
    canActivate: [AuthService]
  },
  {
    path: 'categorias',
    loadChildren: () => import('./Modules/categorias/categorias.module').then(m => m.CategoriasModule),
    canActivate: [AuthService]
  },
  {
    path: 'libros',
    loadChildren: () => import('./Modules/libros/libros.module').then(m => m.LibrosModule),
    canActivate: [AuthService]
  },
  {
    path: 'editoriales',
    loadChildren: () => import('./Modules/editoriales/editoriales.module').then(m => m.EditorialesModule),
    canActivate: [AuthService]
  },
  {
    path: 'carreras',
    loadChildren: () => import('./Modules/carreras/carreras.module').then(m => m.CarrerasModule),
    canActivate: [AuthService]
  },
  {
    path: 'prestamos',
    loadChildren: () => import('./Modules/prestamos/prestamos.module').then(m => m.PrestamosModule),
    canActivate: [AuthService]
  },
  {
    path: 'control-prestamos',
    loadChildren: () => import('./Modules/control-prestamos/control-prestamos.module').then(m => m.ControlPrestamosModule),
    canActivate: [AuthService]
  },
  {
    path: 'solicitar-prestamo',
    loadChildren: () => import('./Modules/solicitar-prestamo/solicitar-prestamo.module').then(m => m.SolicitarPrestamoModule),
    canActivate: [AuthService]
  },
  {
    path: 'usuario',
    loadChildren: () => import('./Modules/administrador/administrador.module').then(m => m.AdministradorModule),
    canActivate: [AuthService]
  },
  {
    path: 'nuevo-usuario',
    loadChildren: () => import('./Modules/nuevo-usuario/nuevo-usuario.module').then(m => m.NuevoUsuarioModule),
    canActivate: [AuthService]
  },

  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
