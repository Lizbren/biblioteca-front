import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import Swal from 'sweetalert2';

@Injectable({
    providedIn: 'root'
})
export class AuthService implements CanActivate {

    constructor(
        readonly router: Router
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
        let logged = sessionStorage.getItem('logged')
        if (!logged) {
            Swal.fire({
                title: 'No estás logueado',
                text: 'Ingresa tus credenciales',
                icon: 'error'
            })
            this.router.navigateByUrl('')
            return false
        } else
            return true
    }

}
