import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AnonymousService implements CanActivate {

    constructor(
        readonly router: Router
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
        let logged = sessionStorage.getItem('logged')
        if (logged) {
            this.router.navigateByUrl('home')
            return false
        } else
            return true
    }

}