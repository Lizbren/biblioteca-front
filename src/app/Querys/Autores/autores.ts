import gql from 'graphql-tag';

export const NUEVO_AUTOR = gql`mutation nuevoAutor($nombre: String!, $fechaNacimiento: String!, $pais: String!) {
    nuevoAutor(nombre: $nombre, fechaNacimiento: $fechaNacimiento, pais: $pais)
  }`;

  export const OBTENER_AUTORES = gql`{
    autores {
      idAutor
      nombre
      fechaNacimiento
      pais
      vigente
    }
  }`;

  export const ACTUALIZAR_AUTOR = gql`mutation actualizaAutor($autor: Autor!) {
    actualizaAutor(autor: $autor)
  }`;

  export const ACTIVAR_AUTOR = gql`mutation actualizaVigenciaAutor($idAutor: Int!, $vigente: Boolean!) {
    actualizaVigenciaAutor(idAutor: $idAutor, vigente: $vigente)
  }`;
  