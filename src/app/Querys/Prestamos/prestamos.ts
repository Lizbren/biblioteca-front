import gql from 'graphql-tag';

export const SOLICITAR_PRESTAMO = gql`mutation solicitarPrestamo($solicitud: Solicitud!) {
  solicitarPrestamo(solicitud: $solicitud)
}`;

export const OBTENER_PRESTAMOS_POR_USUARIO = gql`query prestamosPorUsuario($idUsuario: Int!) {
  prestamosPorUsuario(idUsuario: $idUsuario) {
    idPrestamo
    vencido
    cuota
    autorizado
    fechaPrestamo
    fechaLimite
    renovaciones
    fechaApartado
    titulo
    isbn
    paginas
    nombre
    nombreAdministrador
    tipoUsuario
    carrera
    semestre
    edicion
    editorial
    fechaDevolucion
    matricula
  }
}`;

export const OBTENER_PRESTAMOS = gql`query {
  prestamos {
    idPrestamo
    vencido
    cuota
    autorizado
    fechaPrestamo
    fechaLimite
    renovaciones
    fechaApartado
    titulo
    isbn
    paginas
    nombre
    nombreAdministrador
    tipoUsuario
    matricula
    carrera
    semestre
    edicion
    editorial
    fechaDevolucion
  }}`;

export const AUTORIZAR_PRESTAMO = gql`mutation autorizarPrestamo($idPrestamo: Int!, $idAdministrador: Int!) {
  autorizarPrestamo(idPrestamo: $idPrestamo, idAdministrador: $idAdministrador)
}`;

export const ACTUALIZAR_DEUDAS = gql`mutation {
  actualizarDeudasUsuarios
}`;

export const ACTUALIZAR_VENCIMIENTO = gql`mutation {
  actualizarVencimientoSolicitudes
}`;
export const ACTUALIZAR_DEUDA_POR_USUARIO = gql`mutation actualizarDeudaPorUsuario($idUsuario: Int!) {
  actualizarDeudaPorUsuario(idUsuario: $idUsuario)
}`

export const ACTUALIZAR_VENCIMIENTO_POR_USUARIO = gql`mutation actualizarVencimientoSolicitudPorUsuario($idUsuario: Int!) {
  actualizarVencimientoSolicitudPorUsuario(idUsuario: $idUsuario)
}`
export const RENOVAR_PRESTAMO = gql`mutation renovarPrestamo($idPrestamo: Int!) {
  renovarPrestamo(idPrestamo: $idPrestamo)
}`

export const FINALIZAR_PRESTAMOS = gql`mutation finalizarPrestamo($idPrestamo: Int!) {
  finalizarPrestamo(idPrestamo: $idPrestamo)
}`

