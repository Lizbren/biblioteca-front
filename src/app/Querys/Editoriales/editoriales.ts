import gql from 'graphql-tag';

export const NUEVA_EDITORIAL = gql`mutation nuevaEditorial($editorial: Catalago!) {
  nuevaEditorial(editorial: $editorial)
}`

export const OBTENER_EDITORIAL = gql`{
  editoriales {
    idEditorial
    nombreMay
    nombreMin
    vigente
  }
}`;

export const ACTUALIZAR_EDITORIAL = gql`mutation actualizaEditorial($editorial: Editorial!) {
  actualizaEditorial(editorial: $editorial)
}`;

export const ACTIVAR_EDITORIAL = gql`mutation actualizaVigenciaEditorial($idEditorial: Int!, $vigente: Boolean!) {
  actualizaVigenciaEditorial(idEditorial: $idEditorial, vigente: $vigente)
}`;

export const NO_AUTORIZAR = gql`mutation noAutorizarPrestamo($idPrestamo: Int!) {
  noAutorizarPrestamo(idPrestamo: $idPrestamo)
}`;
