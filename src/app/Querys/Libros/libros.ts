import gql from 'graphql-tag';

export const NUEVO_LIBRO = gql`mutation nuevoLibro($libro: Libro!) {
  nuevoLibro(libro: $libro)
}`

export const OBTENER_LIBROS = gql`{
  libros {
    idLibro
    titulo
    isbn
    paginas
    existencia
    edicion
    editorial
    idEditorial
    categorias
    idCategorias
    autores
    idAutores
    disponibles
    prestados
    apartados
  }
}`;

export const ACTUALIZAR_LIBRO = gql`mutation actualizaLibro($libro: Libro! $idLibro: Int!) {
  actualizaLibro(libro: $libro idLibro: $idLibro)
}`;

export const ACTIVAR_LIBRO = gql`mutation actualizaVigenciaEditorial($idEditorial: Int!, $vigente: Boolean!) {
  actualizaVigenciaEditorial(idEditorial: $idEditorial, vigente: $vigente)
}`;