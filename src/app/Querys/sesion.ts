import gql from 'graphql-tag';

export const LOGIN_USUARIO = gql`query loginUsuario($credenciales: Credenciales!) {
  loginUsuario(usuario: $credenciales) {
    idUsuario
    nombres
    primerApellido
    segundoApellido
    matricula
    vigencia
    idTipoUsuario
    fechaAlta
    cicloActual
    permisos
    tipoUsuario
    correo
    carrera
    semestre
  }
}`;

export  const ACTUALIZA_CLAVE =gql`mutation actualizarClave($clave: String!, $claveAnterior: String!, $idUsuario: Int!) {
  actualizarClave(clave: $clave, claveAnterior: $claveAnterior, idUsuario: $idUsuario)
}`;

export const NUEVO_USUARIO =gql`mutation nuevoUsuario($usuario: UsuarioI!) {
  nuevoUsuario(usuario: $usuario)
}`;

export const OBTENER_USUARIOS =gql`{
  usuarios {
    idUsuario
    nombres
    primerApellido
    segundoApellido
    matricula
    vigencia
    idTipoUsuario
    fechaAlta
    cicloActual
    permisos
    tipoUsuario
    correo
    carrera
    semestre
  }
}`;