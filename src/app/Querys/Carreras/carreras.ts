import gql from 'graphql-tag';

export const NUEVA_CARRERA = gql`mutation nuevaCarrera($carrera: Catalago!) {
  nuevaCarrera(carrera: $carrera)
}`

export const OBTENER_CARRERA = gql`{
  carreras {
    idCarrera
    nombreMay
    nombreMin
    vigente
  }
}`;

export const ACTUALIZAR_CARRERA = gql`mutation actualizaCarrera($carrera: Carrera!) {
  actualizaCarrera(carrera: $carrera)
}`;

export const ACTIVAR_CARRERA = gql`mutation actualizaVigenciaCarrera($idCarrera: Int!, $vigente: Boolean!) {
  actualizaVigenciaCarrera(idCarrera: $idCarrera, vigente: $vigente)
}`;