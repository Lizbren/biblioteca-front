import gql from 'graphql-tag';

export const NUEVA_CATEGORIA = gql`mutation nuevaCategoria($categoria: Catalago!) {
    nuevaCategoria(categoria: $categoria)
}`

export const OBTENER_CATEGORIA = gql`{
  categorias {
    idCategoria
    nombreMay
    nombreMin
    vigente
  }
}`;

export const ACTUALIZAR_CATEGORIA = gql`mutation actualizaCategoria($categoria: Categoria!) {
  actualizaCategoria(categoria: $categoria)
}`;

export const ACTIVAR_CATEGORIA = gql`mutation actualizaVigenciaCategoria($idCategoria: Int!, $vigente: Boolean!) {
  actualizaVigenciaCategoria(idCategoria: $idCategoria, vigente: $vigente)
}`;