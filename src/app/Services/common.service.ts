import {Injectable} from '@angular/core';
import {Apollo} from 'apollo-angular';
import {HttpLink} from 'apollo-angular/http';
import {InMemoryCache} from '@apollo/client/core';
import {environment} from 'src/environments/environment';
import {ACTUALIZAR_AUTOR, NUEVO_AUTOR, OBTENER_AUTORES, ACTIVAR_AUTOR} from '../Querys/Autores/autores';
import {
  NUEVA_CATEGORIA,
  OBTENER_CATEGORIA,
  ACTUALIZAR_CATEGORIA,
  ACTIVAR_CATEGORIA
} from '../Querys/Categorias/categorias';
import {
  NUEVA_EDITORIAL,
  OBTENER_EDITORIAL,
  ACTUALIZAR_EDITORIAL,
  NO_AUTORIZAR
} from '../Querys/Editoriales/editoriales';
import {NUEVA_CARRERA, OBTENER_CARRERA, ACTUALIZAR_CARRERA, ACTIVAR_CARRERA} from '../Querys/Carreras/carreras';
import {NUEVO_LIBRO, OBTENER_LIBROS, ACTUALIZAR_LIBRO} from '../Querys/Libros/libros';
import {
  SOLICITAR_PRESTAMO,
  OBTENER_PRESTAMOS_POR_USUARIO,
  OBTENER_PRESTAMOS,
  AUTORIZAR_PRESTAMO,
  ACTUALIZAR_DEUDAS,
  ACTUALIZAR_VENCIMIENTO,
  ACTUALIZAR_DEUDA_POR_USUARIO, ACTUALIZAR_VENCIMIENTO_POR_USUARIO, RENOVAR_PRESTAMO, FINALIZAR_PRESTAMOS
} from '../Querys/Prestamos/prestamos';
import {ACTUALIZA_CLAVE, NUEVO_USUARIO, OBTENER_USUARIOS} from "../Querys/sesion";

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(
    private apollo: Apollo,
    private httpLink: HttpLink
  ) {
    apollo.create({
      link: httpLink.create({uri: environment.BIBLIOTECA}),
      cache: new InMemoryCache()
    }, 'biblioteca');
  }

  /* AUTORES */
  nuevoAutor(nombre: string, fechaNacimiento: string, pais: string) {
    return this.apollo.use('biblioteca').watchQuery({
      query: NUEVO_AUTOR,
      variables: {
        nombre,
        fechaNacimiento,
        pais
      },
      fetchPolicy: 'no-cache'
    });
  }

  obtenerAutores() {
    return this.apollo.use('biblioteca').watchQuery({
      query: OBTENER_AUTORES,
      fetchPolicy: 'no-cache'
    });
  }

  actualizaAutor(autor: any) {
    return this.apollo.use('biblioteca').watchQuery({
      query: ACTUALIZAR_AUTOR,
      variables: {
        autor
      },
      fetchPolicy: 'no-cache'
    });
  }

  activarAutor(idAutor: Number, vigente: Boolean) {
    return this.apollo.use('biblioteca').watchQuery({
      query: ACTIVAR_AUTOR,
      variables: {
        idAutor,
        vigente
      },
      fetchPolicy: 'no-cache'
    })
  }

  /* CATEGORIAS */

  nuevaCategoria(categoria: any) {
    return this.apollo.use('biblioteca').watchQuery({
      query: NUEVA_CATEGORIA,
      variables: {
        categoria
      },
      fetchPolicy: 'no-cache'
    })
  }

  obtenerCategoria() {
    return this.apollo.use('biblioteca').watchQuery({
      query: OBTENER_CATEGORIA,
      fetchPolicy: 'no-cache'
    });
  }

  actualizarCategoria(categoria: any) {
    return this.apollo.use('biblioteca').watchQuery({
      query: ACTUALIZAR_CATEGORIA,
      variables: {
        categoria
      },
      fetchPolicy: 'no-cache'
    })
  }

  activarCategoria(idCategoria: number, vigente: boolean) {
    return this.apollo.use('biblioteca').watchQuery({
      query: ACTIVAR_CATEGORIA,
      variables: {
        idCategoria,
        vigente
      },
      fetchPolicy: 'no-cache'
    })
  }

  /* EDITORIALES */

  nuevaEditorial(editorial: any) {
    return this.apollo.use('biblioteca').watchQuery({
      query: NUEVA_EDITORIAL,
      variables: {
        editorial
      },
      fetchPolicy: 'no-cache'
    })
  }

  obtenerEditorial() {
    return this.apollo.use('biblioteca').watchQuery({
      query: OBTENER_EDITORIAL,
      fetchPolicy: 'no-cache'
    });
  }

  actualizarEditorial(editorial: any) {
    return this.apollo.use('biblioteca').watchQuery({
      query: ACTUALIZAR_EDITORIAL,
      variables: {
        editorial
      },
      fetchPolicy: 'no-cache'
    })
  }

  /* CARRERAS */

  nuevaCarrera(carrera: any) {
    return this.apollo.use('biblioteca').watchQuery({
      query: NUEVA_CARRERA,
      variables: {
        carrera
      },
      fetchPolicy: 'no-cache'
    });
  }

  obtenerCarrera() {
    return this.apollo.use('biblioteca').watchQuery({
      query: OBTENER_CARRERA,
      fetchPolicy: 'no-cache'
    });
  }

  actualizarCarrera(carrera: any) {
    return this.apollo.use('biblioteca').watchQuery({
      query: ACTUALIZAR_CARRERA,
      variables: {
        carrera
      },
      fetchPolicy: 'no-cache'
    })
  }

  activarCarrera(idCarrera: number, vigente: boolean) {
    return this.apollo.use('biblioteca').watchQuery({
      query: ACTIVAR_CARRERA,
      variables: {
        idCarrera,
        vigente
      },
      fetchPolicy: 'no-cache'
    })
  }

  /* LIBROS */

  nuevoLibro(libro: any) {
    return this.apollo.use('biblioteca').watchQuery({
      query: NUEVO_LIBRO,
      variables: {
        libro
      },
      fetchPolicy: 'no-cache'
    })
  }

  obtenerLibros() {
    return this.apollo.use('biblioteca').watchQuery({
      query: OBTENER_LIBROS,
      fetchPolicy: 'no-cache'
    });
  }

  actualizarLibro(libro: any, idLibro: number) {
    return this.apollo.use('biblioteca').watchQuery({
      query: ACTUALIZAR_LIBRO,
      variables: {
        libro,
        idLibro
      },
      fetchPolicy: 'no-cache'
    })
  }

  /* PRESTAMOS */

  solicitarPrestamo(solicitud: any) {
    return this.apollo.use('biblioteca').watchQuery({
      query: SOLICITAR_PRESTAMO,
      variables: {
        solicitud
      },
      fetchPolicy: 'no-cache'
    })
  }

  obtenerPrestamosPorUsuario(idUsuario: number) {
    return this.apollo.use('biblioteca').watchQuery({
      query: OBTENER_PRESTAMOS_POR_USUARIO,
      variables: {
        idUsuario
      },
      fetchPolicy: 'no-cache'
    })
  }

  obtenerPrestamos() {
    return this.apollo.use('biblioteca').watchQuery({
      query: OBTENER_PRESTAMOS,
      fetchPolicy: 'no-cache'
    })
  }

  autorizarPrestamo(idPrestamo: number, idAdministrador: number) {
    return this.apollo.use('biblioteca').watchQuery({
      query: AUTORIZAR_PRESTAMO,
      variables: {
        idPrestamo,
        idAdministrador
      },
      fetchPolicy: 'no-cache'
    })
  }

  actualizarDeudas() {
    return this.apollo.use('biblioteca').watchQuery({
      query: ACTUALIZAR_DEUDAS,
      fetchPolicy: 'no-cache'
    })
  }

  actualizarVencimiento() {
    return this.apollo.use('biblioteca').watchQuery({
      query: ACTUALIZAR_VENCIMIENTO,
      fetchPolicy: 'no-cache'
    })
  }

  actualizarDeudaPorUsuario(idUsuario: number) {
    return this.apollo.use('biblioteca').watchQuery({
      query: ACTUALIZAR_DEUDA_POR_USUARIO,
      variables: {
        idUsuario
      },
      fetchPolicy: 'no-cache'
    })
  }

  actualizarVencimientoPorUsuario(idUsuario: number) {
    return this.apollo.use('biblioteca').watchQuery({
      query: ACTUALIZAR_VENCIMIENTO_POR_USUARIO,
      variables: {
        idUsuario
      },
      fetchPolicy: 'no-cache'
    })
  }

  renovarPrestamo(idPrestamo: number) {
    return this.apollo.use('biblioteca').watchQuery({
      query: RENOVAR_PRESTAMO,
      variables: {
        idPrestamo
      },
      fetchPolicy: 'no-cache'
    })
  }

  finalizarPrestamo(idPrestamo: number) {
    return this.apollo.use('biblioteca').watchQuery({
      query: FINALIZAR_PRESTAMOS,
      variables: {
        idPrestamo
      },
      fetchPolicy: 'no-cache'
    })
  }

  noAutorizarPrestamo(idPrestamo: number) {
    return this.apollo.use('biblioteca').watchQuery({
      query: NO_AUTORIZAR,
      variables: {
        idPrestamo
      },
      fetchPolicy: 'no-cache'
    })
  }

  actualizarClave(clave: string, claveAnterior: string, idUsuario: number) {
    return this.apollo.use('biblioteca').watchQuery({
      query: ACTUALIZA_CLAVE,
      variables: {
        clave,
        claveAnterior,
        idUsuario
      },
      fetchPolicy: 'no-cache'
    })
  }

  nuevoUsuario(usuario: any) {
    return this.apollo.use('biblioteca').watchQuery({
      query: NUEVO_USUARIO,
      variables: {
        usuario
      },
      fetchPolicy: 'no-cache'
    })
  }

  obtenerUsuarios() {
    return this.apollo.use('biblioteca').watchQuery({
      query: OBTENER_USUARIOS,
      fetchPolicy: 'no-cache'
    })
  }
}
