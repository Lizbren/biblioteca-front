import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AutoresRoutingModule } from './autores-routing.module'
import { AutoresComponent } from './autores.component';

import { SharedModule } from '../shared.module';
@NgModule({
  declarations: [AutoresComponent],
  imports: [
    CommonModule,
    AutoresRoutingModule,
    SharedModule
  ]
})
export class AutoresModule {

}
