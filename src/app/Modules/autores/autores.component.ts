import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CommonService} from '../../Services/common.service'
import Swal from 'sweetalert2';
import {PageEvent} from "@angular/material/paginator";
import {formatDate} from "@angular/common";

@Component({
  selector: 'app-autores',
  templateUrl: './autores.component.html',
  styleUrls: ['./autores.component.scss']
})
export class AutoresComponent {

  hoy = new Date();
  nuevoAutorForm: FormGroup;
  modificaAutorForm: FormGroup;
  autores: Array<any> = [];
  nombresFiltrados = this.autores.slice();
  selectedAutor: any = {
    idAutor: 0,
    nombre: '',
    fechaNacimiento: '',
    pais: '',
    vigente: false
  };
  buscar: string = '';
  busqueda: Array<any> = [];
  length = 0;
  pageSize = 20;
  pageSizeOptions: number[] = [10, 30, 50, 100];
  pageEvent: PageEvent = new PageEvent();
  vigencia: string = 'true';

  constructor(readonly autoresService: CommonService) {
    this.nuevoAutorForm = new FormGroup({
      autor: new FormControl('', [Validators.required]),
      fechaNacimiento: new FormControl({value: '', disabled: true}, [Validators.required]),
      pais: new FormControl('', [Validators.required])
    });

    this.modificaAutorForm = new FormGroup({
      mIdAutor: new FormControl({value: '', disabled: true}, [Validators.required]),
      mAutor: new FormControl('', [Validators.required]),
      mFechaNacimiento: new FormControl({value: '', disabled: true}, [Validators.required]),
      mPais: new FormControl('', [Validators.required])
    });

    this.obtenerAutores();
  }

  get autor() {
    return this.nuevoAutorForm.controls.autor;
  }

  get fechaNacimiento() {
    return this.nuevoAutorForm.controls.fechaNacimiento;
  }

  get pais() {
    return this.nuevoAutorForm.controls.pais;
  }

  get mIdAutor() {
    return this.modificaAutorForm.controls.mIdAutor;
  }

  get mAutor() {
    return this.modificaAutorForm.controls.mAutor;
  }

  get mFechaNacimiento() {
    return this.modificaAutorForm.controls.mFechaNacimiento;
  }

  get mPais() {
    return this.modificaAutorForm.controls.mPais;
  }

  obtenerAutores() {
    this.autoresService.obtenerAutores()
      .result()
      .then((autores: any) => {
        this.autores = autores.data['autores'];
        this.nombresFiltrados = this.autores.slice();
        this.busqueda = this.autores;
        this.pageEvent.length = this.busqueda.length;
        this.pageEvent.pageSize = this.pageSize;
        this.pageEvent.pageIndex = 0;
        this.length = this.busqueda.length;
        if (this.autores.length > 0)
          this.buscarAutor();
      }).catch(() => {
      Swal.fire(
        'Fallo!',
        'No hemos podido obtener información sobre los autores!',
        'error'
      );
    });
  }

  nuevoAutor(): void {
    Swal.fire({
      title: 'Registrando autor',
      didOpen: () => {
        Swal.showLoading();
      }
    });
    const locale = 'en-EN';
    this.autoresService.nuevoAutor(this.autor.value, formatDate(this.fechaNacimiento.value, 'yyyy-MM-dd', locale), this.pais.value)
      .result()
      .then((nuevoAutor: any) => {
        if (nuevoAutor['data'].nuevoAutor === -1)
          Swal.fire(
            'Nuevo Autor!',
            'Al parecer este autor ya esta registrado!',
            'warning'
          ).then(() => this.obtenerAutores());
        else {
          this.limpiarFormularioNuevo();
          Swal.fire(
            'Completo!',
            'Autor registrado correctamente!',
            'success'
          ).then(() => this.obtenerAutores());
        }
      })
      .catch(() => {
        Swal.fire(
          'Fallo!',
          'No hemos podido registrar al autor!',
          'error'
        );
      });

  }

  onChange() {
    let fecha =this.selectedAutor.fechaNacimiento.split('-');
    let fechaUpdate = new Date(parseInt(fecha[0],10), parseInt(fecha[1],10) - 1, parseInt(fecha[2],10))
    this.modificaAutorForm.controls.mIdAutor.setValue(this.selectedAutor.idAutor);
    this.modificaAutorForm.controls.mAutor.setValue(this.selectedAutor.nombre);
    this.modificaAutorForm.controls.mFechaNacimiento.setValue(fechaUpdate);
    this.modificaAutorForm.controls.mPais.setValue(this.selectedAutor.pais);
  }

  actualizarAutor(): void {
    const autor = {
      idAutor: this.mIdAutor.value,
      nombre: this.mAutor.value,
      fechaNacimiento: this.mFechaNacimiento.value,
      pais: this.mPais.value
    };

    Swal.fire({
      title: '¿Deseas continuar con la actualización?',
      showLoaderOnConfirm: true,
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Actualizar',
      allowEscapeKey: false,
      allowOutsideClick: false,
      allowEnterKey: false,
      preConfirm: () => {
        return this.autoresService.actualizaAutor(autor).result()
          .then((result: any) => result['data'].actualizaAutor)
          .catch(() => {
            Swal.showValidationMessage('No hemos podido actualizar al autor');
          });
      },
    }).then((result) => {
      if (result.isConfirmed) {
        if (result.value === -1)
          Swal.fire(
            'Actualiza Autor!',
            'Los datos para actualizar son iguales a un autor ya registrado!!',
            'warning'
          ).then(() => this.obtenerAutores());
        else {
          this.limpiarFormularioActualizar();
          Swal.fire(
            'Completo!',
            'Autor actualizado correctamente!',
            'success'
          ).then(() => this.obtenerAutores());
        }
      }
    });
  }

  vigenciaAutor() {
    let msg = this.selectedAutor.vigente ? 'desactivar' : 'activar';
    Swal.fire({
      title: `¿Deseas ${msg} este autor?`,
      showLoaderOnConfirm: true,
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Confirmar',
      allowEscapeKey: false,
      allowOutsideClick: false,
      allowEnterKey: false,
      preConfirm: () => {
        return this.autoresService.activarAutor(this.selectedAutor.idAutor, !this.selectedAutor.vigente).result()
          .then(() => {
            this.selectedAutor.vigente = !this.selectedAutor.vigente;
            return true;
          })
          .catch(() => {
            Swal.showValidationMessage('No hemos podido actualizar la categoría');
          });
      },
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: `Se actualizó correctamente`,
          icon: 'success'
        });
        this.obtenerAutores();
      }
    });
  }

  buscarAutor() {
    this.busqueda = [];
    if (this.vigencia === 'todos') {
      if (this.buscar === '')
        this.busqueda = this.autores;
      else {
        this.autores.filter((filtrados: any) =>
          (filtrados.nombre.toLowerCase().includes(this.buscar.toLowerCase()) ||
            filtrados.pais.toLowerCase().includes(this.buscar.toLowerCase()) ||
            filtrados.fechaNacimiento.toLowerCase().includes(this.buscar.toLowerCase())))
          .forEach(resultado => this.busqueda.push(resultado));
      }
    } else {
      let truthValue = (this.vigencia === 'true');
      if (this.buscar === '')
        this.autores.filter((filtrados: any) => truthValue === filtrados.vigente)
          .forEach(resultado => this.busqueda.push(resultado));
      else
        this.autores.filter((filtrados: any) =>
          (filtrados.nombre.toLowerCase().includes(this.buscar.toLowerCase()) ||
            filtrados.pais.toLowerCase().includes(this.buscar.toLowerCase()) ||
            filtrados.fechaNacimiento.toLowerCase().includes(this.buscar.toLowerCase())) && truthValue === filtrados.vigente)
          .forEach(resultado => this.busqueda.push(resultado));
    }
    this.pageEvent.length = this.busqueda.length;
    this.pageEvent.pageSize = this.pageSize;
    this.pageEvent.pageIndex = 0;
    this.length = this.busqueda.length;
  }

  limpiarFormularioActualizar() {
    this.modificaAutorForm = new FormGroup({
      mIdAutor: new FormControl({value: '', disabled: true}, [Validators.required]),
      mAutor: new FormControl('', [Validators.required]),
      mFechaNacimiento: new FormControl({value: '', disabled: true}, [Validators.required]),
      mPais: new FormControl('', [Validators.required])
    });
  }

  limpiarFormularioNuevo() {
    this.nuevoAutorForm = new FormGroup({
      autor: new FormControl('', [Validators.required]),
      fechaNacimiento: new FormControl({value: '', disabled: true}, [Validators.required]),
      pais: new FormControl('', [Validators.required])
    });

  }
}
