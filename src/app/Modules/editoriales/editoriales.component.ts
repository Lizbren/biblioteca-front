import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CommonService} from '../../Services/common.service'
import Swal from 'sweetalert2';
import {PageEvent} from "@angular/material/paginator";

@Component({
  selector: 'app-editoriales',
  templateUrl: './editoriales.component.html',
  styleUrls: ['./editoriales.component.scss']
})
export class EditorialesComponent {
  selectedEditorial: any = {
    idEditorial: 0,
    nombreMay: '',
    nombreMin: '',
    vigencia: false
  }

  tablaEditoriales: any;
  editoriales: Array<any> = [];
  nombresFiltrados = this.editoriales.slice();
  vigencias = [{nombre: 'Vigente', valor: true}, {nombre: 'No vigente', valor: false}]
  nuevaEditorialForm: FormGroup;
  modificaEditorialForm: FormGroup;
  displayedColumns: string[] = ['ID', 'Nombre Mayusculas', 'Nombre Minusculas', 'Vigencia'];
  buscar: string = '';
  busqueda: Array<any> = [];
  length = 0;
  pageSize = 20;
  pageSizeOptions: number[] = [10, 30, 50, 100];
  pageEvent: PageEvent = new PageEvent();
  vigenciaSelect: string = 'true';

  constructor(readonly editorialService: CommonService) {
    this.nuevaEditorialForm = new FormGroup({
      nombreMay: new FormControl('', [Validators.required]),
      nombreMin: new FormControl({value: '', disabled: true}, [Validators.required]),
      vigencia: new FormControl('', [Validators.required])
    });
    this.modificaEditorialForm = new FormGroup({
      mIdEditorial: new FormControl({value: '', disabled: true}, [Validators.required]),
      mNombreMay: new FormControl('', [Validators.required]),
      mNombreMin: new FormControl({value: '', disabled: true}, [Validators.required])
    });

    this.obtenerEditoriales();
  }

  get nombreMay() {
    return this.nuevaEditorialForm.controls.nombreMay;
  }

  get nombreMin() {
    return this.nuevaEditorialForm.controls.nombreMin;
  }

  get vigencia() {
    return this.nuevaEditorialForm.controls.vigencia;
  }

  get mIdEditorial() {
    return this.modificaEditorialForm.controls.mIdEditorial;
  }

  get mNombreMay() {
    return this.modificaEditorialForm.controls.mNombreMay;
  }

  get mNombreMin() {
    return this.modificaEditorialForm.controls.mNombreMin;
  }

  onChange() {
    this.modificaEditorialForm.controls.mIdEditorial.setValue(this.selectedEditorial.idEditorial);
    this.modificaEditorialForm.controls.mNombreMay.setValue(this.selectedEditorial.nombreMay);
    this.modificaEditorialForm.controls.mNombreMin.setValue(this.selectedEditorial.nombreMin);
  }

  nuevaEditorial(): void {
    const editorial = {
      nombreMay: this.nombreMay.value.toUpperCase(),
      nombreMin: this.nombreMin.value.toLowerCase(),
      vigente: this.vigencia.value
    }
    this.editorialService.nuevaEditorial(editorial).result()
      .then((nuevaEditorial: any) => {
        if (nuevaEditorial['data'].nuevaEditorial === -1)
          Swal.fire(
            'Nueva Editorial!',
            'Al parecer esta editorial ya esta registrada!',
            'warning'
          ).then(() => this.obtenerEditoriales());
        else {
          this.limpiarFormularioNuevo();
          Swal.fire(
            'Completo!',
            'Editorial registrada correctamente!',
            'success'
          ).then(() => this.obtenerEditoriales());
        }
      })
      .catch(() => {
        Swal.fire(
          'Fallo!',
          'No hemos podido registrar la editorial!',
          'error'
        );
      });
  }

  obtenerEditoriales() {
    this.editorialService.obtenerEditorial().result()
      .then((editoriales: any) => {
        this.editoriales = editoriales.data['editoriales'];
        this.nombresFiltrados = this.editoriales.slice();
        this.busqueda = this.editoriales;
        this.pageEvent.length = this.busqueda.length;
        this.pageEvent.pageSize = this.pageSize;
        this.pageEvent.pageIndex = 0;
        this.length = this.busqueda.length;
        if (this.editoriales.length > 0)
          this.buscarEditorial();
      }).catch(() => {
      Swal.fire(
        'Fallo!',
        'No hemos podido obtener información sobre los editoriales!',
        'error'
      );
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tablaEditoriales.filter = filterValue.trim().toLowerCase();
  }

  onKey(event: any) { // without type info
    this.nombreMay.setValue(event.target.value.toUpperCase());
    this.nombreMin.setValue(event.target.value.toLowerCase());
  }

  actualizarEditorial(): void {
    const editorial = {
      idEditorial: this.mIdEditorial.value,
      nombreMay: this.mNombreMay.value.toUpperCase(),
      nombreMin: this.mNombreMay.value.toLowerCase()
    };

    Swal.fire({
      title: '¿Deseas continuar con la actualización?',
      showLoaderOnConfirm: true,
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Actualizar',
      allowEscapeKey: false,
      allowOutsideClick: false,
      allowEnterKey: false,
      preConfirm: () => {
        return this.editorialService.actualizarEditorial(editorial).result()
          .then((result: any) => result['data'].actualizaEditorial)
          .catch(() => Swal.showValidationMessage('No hemos podido actualiza la editorial'));
      },
    }).then((result) => {
      if (result.isConfirmed) {
        if (result.value === -1)
          Swal.fire(
            'Actualiza Editorial!',
            'Los datos para actualizar son iguales a una editorial ya registrada!!',
            'warning'
          ).then(() => this.obtenerEditoriales());
        else {
          this.limpiarFormularioActualizar();
          Swal.fire(
            'Completo!',
            'Editorial actualizada correctamente!',
            'success'
          ).then(() => this.obtenerEditoriales());
        }
      }
    });
  }

  buscarEditorial() {
    this.busqueda = [];
    if (this.vigenciaSelect === 'todos') {
      if (this.buscar === '')
        this.busqueda = this.editoriales;
      else {
        this.editoriales.filter((filtrados: any) => filtrados.nombreMay.toLowerCase().includes(this.buscar.toLowerCase()))
          .forEach(resultado => this.busqueda.push(resultado));
      }
    } else {
      let truthValue = (this.vigenciaSelect === 'true');
      if (this.buscar === '')
        this.editoriales.filter((filtrados: any) => truthValue === filtrados.vigente)
          .forEach(resultado => this.busqueda.push(resultado));
      else
        this.editoriales.filter((filtrados: any) => filtrados.nombreMay.toLowerCase().includes(this.buscar.toLowerCase()) && truthValue === filtrados.vigente)
          .forEach(resultado => this.busqueda.push(resultado));
    }
    this.pageEvent.length = this.busqueda.length;
    this.pageEvent.pageSize = this.pageSize;
    this.pageEvent.pageIndex = 0;
    this.length = this.busqueda.length;
  }

  limpiarFormularioActualizar() {
    this.modificaEditorialForm = new FormGroup({
      mIdEditorial: new FormControl({value: '', disabled: true}, [Validators.required]),
      mNombreMay: new FormControl('', [Validators.required]),
      mNombreMin: new FormControl({value: '', disabled: true}, [Validators.required])
    });
  }

  limpiarFormularioNuevo() {
    this.nuevaEditorialForm = new FormGroup({
      nombreMay: new FormControl('', [Validators.required]),
      nombreMin: new FormControl({value: '', disabled: true}, [Validators.required]),
      vigencia: new FormControl('', [Validators.required])
    });
  }
}
