import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditorialesComponent} from './editoriales.component'
const routes: Routes = [
  { path: '', component: EditorialesComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditorialesRoutingModule { }
