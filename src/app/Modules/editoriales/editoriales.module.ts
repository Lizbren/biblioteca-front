import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditorialesRoutingModule } from './editoriales-routing.module';
import { EditorialesComponent } from './editoriales.component';

import { SharedModule } from '../shared.module';

@NgModule({
  declarations: [
    EditorialesComponent
  ],
  imports: [
    CommonModule,
    EditorialesRoutingModule,
    SharedModule
  ]
})
export class EditorialesModule { }
