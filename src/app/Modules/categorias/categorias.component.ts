import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CommonService} from '../../Services/common.service'
import Swal from 'sweetalert2';
import {PageEvent} from "@angular/material/paginator";

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.scss']
})
export class CategoriasComponent {
  selectedCategoria: any = {
    idCategoria: 0,
    nombreMay: '',
    nombreMin: '',
    vigencia: false
  }

  categorias: Array<any> = [];
  nombresFiltrados = this.categorias.slice();
  vigencias = [{nombre: 'Vigente', valor: true}, {nombre: 'No vigente', valor: false}]
  nuevaCategoriaForm: FormGroup;
  modificaCategoriaForm: FormGroup;
  buscar: string = '';
  busqueda: Array<any> = [];
  length = 0;
  pageSize = 20;
  pageSizeOptions: number[] = [10, 30, 50, 100];
  pageEvent: PageEvent = new PageEvent();
  vigenciaSelect: string = 'true';

  constructor(readonly categoriaService: CommonService) {
    this.nuevaCategoriaForm = new FormGroup({
      nombreMay: new FormControl('', [Validators.required]),
      nombreMin: new FormControl({value: '', disabled: true}, [Validators.required]),
      vigencia: new FormControl('', [Validators.required])
    });
    this.modificaCategoriaForm = new FormGroup({
      mIdCategoria: new FormControl({value: '', disabled: true}, [Validators.required]),
      mNombreMay: new FormControl('', [Validators.required]),
      mNombreMin: new FormControl({value: '', disabled: true}, [Validators.required])
    });
    this.obtenerCategorias();
  }

  get nombreMay() {
    return this.nuevaCategoriaForm.controls.nombreMay;
  }

  get nombreMin() {
    return this.nuevaCategoriaForm.controls.nombreMin;
  }

  get vigencia() {
    return this.nuevaCategoriaForm.controls.vigencia;
  }

  get mIdCategoria() {
    return this.modificaCategoriaForm.controls.mIdCategoria;
  }

  get mNombreMay() {
    return this.modificaCategoriaForm.controls.mNombreMay;
  }

  get mNombreMin() {
    return this.modificaCategoriaForm.controls.mNombreMin;
  }

  onChange() {
    this.modificaCategoriaForm.controls.mIdCategoria.setValue(this.selectedCategoria.idCategoria);
    this.modificaCategoriaForm.controls.mNombreMay.setValue(this.selectedCategoria.nombreMay);
    this.modificaCategoriaForm.controls.mNombreMin.setValue(this.selectedCategoria.nombreMin);
  }

  nuevaCategoria(): void {
    const categoria = {
      nombreMay: this.nombreMay.value.toUpperCase(),
      nombreMin: this.nombreMin.value.toLowerCase(),
      vigente: this.vigencia.value
    }
    this.categoriaService.nuevaCategoria(categoria).result()
      .then((nuevaCategoria: any) => {
        if (nuevaCategoria['data'].nuevaCategoria === -1)
          Swal.fire(
            'Nueva Categoria!',
            'Al parecer esta categoria ya esta registrada!',
            'warning'
          ).then(() => this.obtenerCategorias());
        else {
          this.limpiarFormularioNuevo();
          Swal.fire(
            'Completo!',
            'Categoria registrada correctamente!',
            'success'
          ).then(() => this.obtenerCategorias());
        }
      })
      .catch(() => {
        Swal.fire(
          'Fallo!',
          'No hemos podido registrar la categoria!',
          'error'
        );
      });
  }

  obtenerCategorias() {
    this.categoriaService.obtenerCategoria().result()
      .then((categorias: any) => {
        this.categorias = categorias.data['categorias'];
        this.nombresFiltrados = this.categorias.slice();
        this.busqueda = this.categorias;
        this.pageEvent.length = this.busqueda.length;
        this.pageEvent.pageSize = this.pageSize;
        this.pageEvent.pageIndex = 0;
        this.length = this.busqueda.length;
        if (this.categorias.length > 0)
          this.buscarCategoria();
      }).catch(() => {
      Swal.fire(
        'Fallo!',
        'No hemos podido obtener información sobre los categorias!',
        'error'
      );
    });
  }

  onKey(event: any) {
    this.nombreMay.setValue(event.target.value.toUpperCase());
    this.nombreMin.setValue(event.target.value.toLowerCase());
  }

  actualizarCategoria(): void {
    const categoria = {
      idCategoria: this.mIdCategoria.value,
      nombreMay: this.mNombreMay.value.toUpperCase(),
      nombreMin: this.mNombreMay.value.toLowerCase()
    };
    Swal.fire({
      title: '¿Deseas continuar con la actualización?',
      showLoaderOnConfirm: true,
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Actualizar',
      allowEscapeKey: false,
      allowOutsideClick: false,
      allowEnterKey: false,
      preConfirm: () => {
        return this.categoriaService.actualizarCategoria(categoria).result()
          .then((result: any) => result['data'].actualizaCategoria)
          .catch(() => Swal.showValidationMessage('No hemos podido actualiza la categoria'));
      },
    }).then((result) => {
      if (result.isConfirmed) {
        if (result.value === -1)
          Swal.fire(
            'Actualiza Categoria!',
            'Los datos para actualizar son iguales a una categoria ya registrada!!',
            'warning'
          ).then(() => this.obtenerCategorias());
        else {
          this.limpiarFormularioActualizar();
          Swal.fire(
            'Completo!',
            'Categoria actualizada correctamente!',
            'success'
          ).then(() => this.obtenerCategorias());
        }
      }
    });
  }

  vigenciaCategoria() {
    let msg = this.selectedCategoria.vigente ? 'desactivar' : 'activar';
    Swal.fire({
      title: `¿Deseas ${msg} esta categoría?`,
      showLoaderOnConfirm: true,
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Confirmar',
      allowEscapeKey: false,
      allowOutsideClick: false,
      allowEnterKey: false,
      preConfirm: () => {
        return this.categoriaService.activarCategoria(this.selectedCategoria.idCategoria, !this.selectedCategoria.vigente).result()
          .then(() => {
            this.selectedCategoria.vigente = !this.selectedCategoria.vigente;
            return true;
          })
          .catch(() => Swal.showValidationMessage('No hemos podido actualizar la categoría'));
      },
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: `Se actualizó correctamente`,
          icon: 'success'
        }).then(() => this.obtenerCategorias());
      }
    });
  }

  buscarCategoria() {
    this.busqueda = [];
    if (this.vigenciaSelect === 'todos') {
      if (this.buscar === '')
        this.busqueda = this.categorias;
      else {
        this.categorias.filter((filtrados: any) => filtrados.nombreMay.toLowerCase().includes(this.buscar.toLowerCase()))
          .forEach(resultado => this.busqueda.push(resultado));
      }
    } else {
      let truthValue = (this.vigenciaSelect === 'true');
      if (this.buscar === '')
        this.categorias.filter((filtrados: any) => truthValue === filtrados.vigente)
          .forEach(resultado => this.busqueda.push(resultado));
      else
        this.categorias.filter((filtrados: any) => filtrados.nombreMay.toLowerCase().includes(this.buscar.toLowerCase()) && truthValue === filtrados.vigente)
          .forEach(resultado => this.busqueda.push(resultado));
    }
    this.pageEvent.length = this.busqueda.length;
    this.pageEvent.pageSize = this.pageSize;
    this.pageEvent.pageIndex = 0;
    this.length = this.busqueda.length;
  }

  limpiarFormularioActualizar() {
    this.modificaCategoriaForm = new FormGroup({
      mIdCategoria: new FormControl({value: '', disabled: true}, [Validators.required]),
      mNombreMay: new FormControl('', [Validators.required]),
      mNombreMin: new FormControl({value: '', disabled: true}, [Validators.required])
    });
  }

  limpiarFormularioNuevo() {
    this.nuevaCategoriaForm = new FormGroup({
      nombreMay: new FormControl('', [Validators.required]),
      nombreMin: new FormControl({value: '', disabled: true}, [Validators.required]),
      vigencia: new FormControl('', [Validators.required])
    });
  }
}
