import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NuevoUsuarioRoutingModule } from './nuevo-usuario-routing.module';
import { NuevoUsuarioComponent , NuevoUsuarioComponentDialog} from './nuevo-usuario.component';
import {SharedModule} from "../shared.module";


@NgModule({
  declarations: [
    NuevoUsuarioComponent,
    NuevoUsuarioComponentDialog
  ],
  imports: [
    CommonModule,
    NuevoUsuarioRoutingModule,
    SharedModule
  ]
})
export class NuevoUsuarioModule { }
