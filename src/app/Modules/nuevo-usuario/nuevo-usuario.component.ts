import {Component, Inject} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import Swal from "sweetalert2";
import {CommonService} from "../../Services/common.service";
import {UES_IMAGE_64, UMB_IMAGE_64} from "../../Constantes/constantes";
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import {PageEvent} from "@angular/material/paginator";
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material/dialog";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-nuevo-usuario',
  templateUrl: './nuevo-usuario.component.html',
  styleUrls: ['./nuevo-usuario.component.scss']
})
export class NuevoUsuarioComponent {
  nuevoUsuario: FormGroup;
  usuarios: Array<any> = [];
  carreras: Array<any> = [];
  busqueda: Array<any> = [];
  buscar: string = '';
  pageSize = 10;
  pageSizeOptions: number[] = [10, 30, 50, 100];
  pageEvent: PageEvent = new PageEvent();
  length = 0;
  nombresFiltrados = this.carreras.slice();
  constructor(readonly commonService: CommonService,  public dialog: MatDialog) {
    this.nuevoUsuario = new FormGroup({
      nombres: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z ]*')]),
      primerApellido: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z ]*')]),
      segundoApellido: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z ]*')]),
      matricula: new FormControl('', [Validators.required, Validators.maxLength(8),
        Validators.minLength(5),
        Validators.pattern('^[0-9]*')]),
      correo: new FormControl('', [Validators.required, Validators.pattern("[a-zA-Z0-9!#$%&'*_+-]([\\.]?[a-zA-Z0-9!#$%&'*_+-])+@[a-zA-Z0-9]([^@&%$\\/()=?¿!.,:;]|\\d)+[a-zA-Z0-9][\\.][a-zA-Z]{2,4}([\\.][a-zA-Z]{2})?")]),
      idTipoUsuario: new FormControl('', [Validators.required]),
      carrera: new FormControl('', [Validators.required]),
      semestre: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*')])
    });
    this.obtenerUsuarios();
    this.obtenerCarreras();

  }

  openDialog(usuario: any) {
    this.dialog.open(NuevoUsuarioComponentDialog, {
      data: {
        usuario: usuario,
      },
    });
  }

  get nombres() {
    return this.nuevoUsuario.controls.nombres;
  }

  get primerApellido() {
    return this.nuevoUsuario.controls.primerApellido;
  }

  get segundoApellido() {
    return this.nuevoUsuario.controls.segundoApellido;
  }

  get matricula() {
    return this.nuevoUsuario.controls.matricula;
  }

  get correo() {
    return this.nuevoUsuario.controls.correo;
  }

  get idTipoUsuario() {
    return this.nuevoUsuario.controls.idTipoUsuario;
  }

  get carrera() {
    return this.nuevoUsuario.controls.carrera;
  }

  get semestre() {
    return this.nuevoUsuario.controls.semestre;
  }

  registrarNuevoUsuario() {
    Swal.fire({
      title: 'Registrando usuario',
      didOpen: () => {
        Swal.showLoading();
      }
    });
    let nuevoUsuarioData = {
      nombres: this.nombres.value,
      primerApellido: this.primerApellido.value,
      segundoApellido: this.segundoApellido.value,
      matricula: this.matricula.value,
      vigencia: true,
      correo: this.correo.value,
      clave: this.generatePasswordRand(8),
      idTipoUsuario: parseInt(this.idTipoUsuario.value, 10),
      cicloActual: "N/A",
      adicional: {
        carrera: this.carrera.value,
        semestre: this.semestre.value,
      }
    }


    this.commonService.nuevoUsuario(nuevoUsuarioData)
      .result()
      .then((nuevoUsuario: any) => {
        if (nuevoUsuario['data'].nuevoUsuario === -1)
          Swal.fire(
            'Nuevo Usuario!',
            'Al parecer este usuario ya esta registrado!',
            'warning'
          );
        else {
          this.usuarioPDF(nuevoUsuarioData);
          this.limpiarFormularioNuevo();
          Swal.fire(
            'Completo!',
            'Usuario registrado correctamente!',
            'success'
          );
        }
      })
      .catch(() => {
        Swal.fire(
          'Fallo!',
          'No hemos podido registrar al usuario!',
          'error'
        );
      });
  }

  usuarioPDF(usuarioPdf: any) {
    const pdfDefinition: any = {
      background: function (page: any) {
        if (page === 1) {
          return [
            {
              table: {
                headerRows: 1,
                heights: [60],
                widths: ['*', '*'],
                body: [
                  [{
                    margin: [0, 20, 0, 0],
                    alignment: 'right',
                    image: UMB_IMAGE_64,
                    fit: [250, 250],
                    border: [false, false, true, false]
                  },
                    {
                      margin: [0, 20, 0, 0],
                      alignment: 'left',
                      image: UES_IMAGE_64,
                      fit: [250, 250],
                      border: [false, false, false, false]
                    }]
                ]
              },
              margin: [0, 20],
            }
          ];
        }
        return [];
      },

      content: [
        '\n\n\n\n\n\n\n\n\n',
        {
          text: 'REGISTRO DE USUARIO',
          style: 'header',
          alignment: 'center'
        },
        {
          text: 'Datos del alumno',
          style: 'subheader',
          alignment: 'center'
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [{text: 'Nombre:', alignment: 'right'}, usuarioPdf.nombres],
              [{text: 'Primer Apellido:', alignment: 'right'}, usuarioPdf.primerApellido],
              [{text: 'Segundo Apellido:', alignment: 'right'}, usuarioPdf.segundoApellido],
              [{text: 'Matricula:', alignment: 'right'}, usuarioPdf.matricula],
              [{text: 'Correo:', alignment: 'right'}, usuarioPdf.correo],
              [{text: 'Clave:', alignment: 'right'}, usuarioPdf.clave],
              [{text: 'Usuario:', alignment: 'right'}, usuarioPdf.idTipoUsuario],
              [{text: 'Carrera:', alignment: 'right'}, usuarioPdf.adicional.carrera],
              [{text: 'Semestre:', alignment: 'right'}, usuarioPdf.adicional.semestre],
            ]
          },
          layout: 'noBorders'
        }
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10]
        },
        subheader: {
          fontSize: 15,
          bold: true
        },
        tableExample: {
          margin: [0, 5, 0, 15]
        },
      }
    }
    console.log('a')
    const pdf = pdfMake.createPdf(pdfDefinition);
    pdf.open();

  }

  obtenerUsuarios() {
    this.commonService.obtenerUsuarios().result()
      .then((usuarios: any) => {
        this.usuarios = usuarios.data['usuarios'];
        this.usuarios.forEach(r =>{
          r.nombre = `${r.nombres} ${r.primerApellido} ${r.segundoApellido}`
        })
        console.log(this.usuarios)
        this.busqueda = this.usuarios;
        this.pageEvent.length = this.busqueda.length;
        this.pageEvent.pageSize = this.pageSize;
        this.pageEvent.pageIndex = 0;
        this.length = this.busqueda.length;
      }).catch(() => {
      Swal.fire(
        'Fallo!',
        'No hemos podido obtener información sobre los usuarios!',
        'error'
      );
    });
  }

  obtenerCarreras() {
    this.commonService.obtenerCarrera().result()
      .then((carreras: any) => {
        this.carreras = carreras.data['carreras'];
        this.nombresFiltrados = this.carreras.slice();
        this.busqueda = this.carreras;
      }).catch(() => {
      Swal.fire(
        'Fallo!',
        'No hemos podido obtener información sobre los carreras!',
        'error'
      );
    });
  }

  buscarUsuario() {
    this.busqueda = [];
    if (this.buscar === '')
        this.busqueda = this.usuarios;
      else {
        this.usuarios.filter((filtrados: any) => filtrados.nombre.toLowerCase().includes(this.buscar.toLowerCase()) || filtrados.matricula.toLowerCase().includes(this.buscar.toLowerCase()))
          .forEach(resultado => this.busqueda.push(resultado));
      }
    this.pageEvent.length = this.busqueda.length;
    this.pageEvent.pageSize = this.pageSize;
    this.pageEvent.pageIndex = 0;
    this.length = this.busqueda.length;
  }

  generatePasswordRand(length: number) {
    const characters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ".", "-", "_", "$", "&", "#", "@"];
    let pass = "";
    for (let i = 0; i < length; i++) {
      pass += characters[Math.floor(Math.random() * characters.length)];
    }
    return pass;
  }

  limpiarFormularioNuevo() {
    this.nuevoUsuario = new FormGroup({
      nombres: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z ]*')]),
      primerApellido: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z ]*')]),
      segundoApellido: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z ]*')]),
      matricula: new FormControl('', [Validators.required, Validators.maxLength(8),
        Validators.minLength(5),
        Validators.pattern('^[0-9]*')]),
      correo: new FormControl('', [Validators.required, Validators.pattern("[a-zA-Z0-9!#$%&'*_+-]([\\.]?[a-zA-Z0-9!#$%&'*_+-])+@[a-zA-Z0-9]([^@&%$\\/()=?¿!.,:;]|\\d)+[a-zA-Z0-9][\\.][a-zA-Z]{2,4}([\\.][a-zA-Z]{2})?")]),
      idTipoUsuario: new FormControl('', [Validators.required]),
      carrera: new FormControl('', [Validators.required]),
      semestre: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*')])
    });
  }
}

@Component({
  selector: 'card-detalle-usuario',
  templateUrl: 'card-detalle-usuario.html',
})
export class NuevoUsuarioComponentDialog {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }
}