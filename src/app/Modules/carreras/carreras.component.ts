import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CommonService} from '../../Services/common.service'
import Swal from 'sweetalert2';
import {PageEvent} from "@angular/material/paginator";

@Component({
  selector: 'app-carreras',
  templateUrl: './carreras.component.html',
  styleUrls: ['./carreras.component.scss']
})
export class CarrerasComponent {
  selectedCarrera: any = {
    idCarrera: 0,
    nombreMay: '',
    nombreMin: '',
    vigencia: false
  }
  carreras: Array<any> = [];
  nombresFiltrados = this.carreras.slice();
  vigencias = [{nombre: 'Vigente', valor: true}, {nombre: 'No vigente', valor: false}]
  nuevaCarreraForm: FormGroup;
  modificaCarreraForm: FormGroup;
  buscar: string = '';
  busqueda: Array<any> = [];
  length = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 30, 50, 100];
  pageEvent: PageEvent = new PageEvent();
  vigenciaSelect: string = 'true';

  constructor(readonly carreraService: CommonService) {
    this.nuevaCarreraForm = new FormGroup({
      nombreMay: new FormControl('', [Validators.required]),
      nombreMin: new FormControl({value: '', disabled: true}, [Validators.required]),
      vigencia: new FormControl('', [Validators.required])
    });
    this.modificaCarreraForm = new FormGroup({
      mIdCarrera: new FormControl({value: '', disabled: true}, [Validators.required]),
      mNombreMay: new FormControl('', [Validators.required]),
      mNombreMin: new FormControl({value: '', disabled: true}, [Validators.required])
    });

    this.obtenerCarreras();
  }

  get nombreMay() {
    return this.nuevaCarreraForm.controls.nombreMay;
  }

  get nombreMin() {
    return this.nuevaCarreraForm.controls.nombreMin;
  }

  get vigencia() {
    return this.nuevaCarreraForm.controls.vigencia;
  }

  get mIdCarrera() {
    return this.modificaCarreraForm.controls.mIdCarrera;
  }

  get mNombreMay() {
    return this.modificaCarreraForm.controls.mNombreMay;
  }

  get mNombreMin() {
    return this.modificaCarreraForm.controls.mNombreMin;
  }


  onChange() {
    this.modificaCarreraForm.controls.mIdCarrera.setValue(this.selectedCarrera.idCarrera);
    this.modificaCarreraForm.controls.mNombreMay.setValue(this.selectedCarrera.nombreMay);
    this.modificaCarreraForm.controls.mNombreMin.setValue(this.selectedCarrera.nombreMin);
  }

  nuevaCarrera(): void {
    const carrera = {
      nombreMay: this.nombreMay.value.toUpperCase(),
      nombreMin: this.nombreMay.value.toLowerCase(),
      vigente: this.vigencia.value
    }
    this.carreraService.nuevaCarrera(carrera).result()
      .then((nuevaCarrera: any) => {
        console.log(nuevaCarrera['data'].nuevaCarrera);
        if (nuevaCarrera['data'].nuevaCarrera === -1)
          Swal.fire(
            'Nueva Carrera!',
            'Al parecer esta carrera ya esta registrada!',
            'warning'
          ).then(() => this.obtenerCarreras());
        else {
          this.limpiarFormularioNuevo();
          Swal.fire(
            'Completo!',
            'Carrera registrada correctamente!',
            'success'
          ).then(() => this.obtenerCarreras());
        }
      })
      .catch(() => {
        Swal.fire(
          'Fallo!',
          'No hemos podido registrar la carrera!',
          'error'
        );
      });
  }

  obtenerCarreras() {
    this.carreraService.obtenerCarrera().result()
      .then((carreras: any) => {
        this.carreras = carreras.data['carreras'];
        this.nombresFiltrados = this.carreras.slice();
        this.busqueda = this.carreras;
        this.pageEvent.length = this.busqueda.length;
        this.pageEvent.pageSize = this.pageSize;
        this.pageEvent.pageIndex = 0;
        this.length = this.busqueda.length;
        if (this.carreras.length > 0)
          this.buscarCarrera();
      }).catch(() => {
      Swal.fire(
        'Fallo!',
        'No hemos podido obtener información sobre los carreras!',
        'error'
      );
    });
  }

  onKey(event: any) { // without type info
    this.nombreMay.setValue(event.target.value.toUpperCase());
    this.nombreMin.setValue(event.target.value.toLowerCase());
  }

  actualizarCarrera(): void {
    const carrera = {
      idCarrera: this.mIdCarrera.value,
      nombreMay: this.mNombreMay.value.toUpperCase(),
      nombreMin: this.mNombreMay.value.toLowerCase()
    };

    Swal.fire({
      title: '¿Deseas continuar con la actualización?',
      showLoaderOnConfirm: true,
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Actualizar',
      allowEscapeKey: false,
      allowOutsideClick: false,
      allowEnterKey: false,
      preConfirm: () => {
        return this.carreraService.actualizarCarrera(carrera).result()
          .then((result: any) => result['data'].actualizaCarrera)
          .catch(() => {
            Swal.showValidationMessage('No hemos podido actualiza la carrera');
          });
      },
    }).then((result) => {
      if (result.isConfirmed) {
        if (result.value === -1)
          Swal.fire(
            'Actualiza Carrera!',
            'Los datos para actualizar son iguales a una carrera ya registrada!!',
            'warning'
          ).then(() => this.obtenerCarreras());
        else {
          this.limpiarFormularioActualizar();
          Swal.fire(
            'Completo!',
            'Carrera actualizada correctamente!',
            'success'
          ).then(() => this.obtenerCarreras());
        }
      }
    });
  }

  vigenciaCarrera() {
    let msg = this.selectedCarrera.vigente ? 'desactivar' : 'activar';
    Swal.fire({
      title: `¿Deseas ${msg} esta carrera?`,
      showLoaderOnConfirm: true,
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Confirmar',
      allowEscapeKey: false,
      allowOutsideClick: false,
      allowEnterKey: false,
      preConfirm: () => {
        return this.carreraService.activarCarrera(this.selectedCarrera.idCarrera, !this.selectedCarrera.vigente).result()
          .then(() => {
            this.selectedCarrera.vigente = !this.selectedCarrera.vigente;
            return true;
          })
          .catch(() => {
            Swal.showValidationMessage('No hemos podido actualizar la carrera');
          });
      },
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: `Se actualizó correctamente`,
          icon: 'success'
        }).then(() => this.obtenerCarreras());
      }
    });
  }

  buscarCarrera() {
    this.busqueda = [];
    if (this.vigenciaSelect === 'todos') {
      if (this.buscar === '')
        this.busqueda = this.carreras;
      else {
        this.carreras.filter((filtrados: any) => filtrados.nombreMay.toLowerCase().includes(this.buscar.toLowerCase()))
          .forEach(resultado => this.busqueda.push(resultado));
      }
    } else {
      let truthValue = (this.vigenciaSelect === 'true');
      if (this.buscar === '')
        this.carreras.filter((filtrados: any) => truthValue === filtrados.vigente)
          .forEach(resultado => this.busqueda.push(resultado));
      else
        this.carreras.filter((filtrados: any) => filtrados.nombreMay.toLowerCase().includes(this.buscar.toLowerCase()) && truthValue === filtrados.vigente)
          .forEach(resultado => this.busqueda.push(resultado));
    }
    this.pageEvent.length = this.busqueda.length;
    this.pageEvent.pageSize = this.pageSize;
    this.pageEvent.pageIndex = 0;
    this.length = this.busqueda.length;
  }

  limpiarFormularioActualizar() {
    this.modificaCarreraForm = new FormGroup({
      mIdCarrera: new FormControl({value: '', disabled: true}, [Validators.required]),
      mNombreMay: new FormControl('', [Validators.required]),
      mNombreMin: new FormControl({value: '', disabled: true}, [Validators.required])
    });
  }
  limpiarFormularioNuevo() {
    this.nuevaCarreraForm = new FormGroup({
      nombreMay: new FormControl('', [Validators.required]),
      nombreMin: new FormControl({value: '', disabled: true}, [Validators.required]),
      vigencia: new FormControl('', [Validators.required])
    });
  }
}
