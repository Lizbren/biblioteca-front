import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarrerasRoutingModule } from './carreras-routing.module';
import { CarrerasComponent } from './carreras.component';

import { SharedModule } from '../shared.module';

@NgModule({
  declarations: [
    CarrerasComponent
  ],
  imports: [
    CommonModule,
    CarrerasRoutingModule,
    SharedModule
  ]
})
export class CarrerasModule { }
