import {Component} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import Swal from "sweetalert2";
import {CommonService} from "../../Services/common.service";

@Component({
  selector: 'app-administrador',
  templateUrl: './administrador.component.html',
  styleUrls: ['./administrador.component.scss']
})
export class AdministradorComponent {
  usuario: {
    idUsuario: number
    tipoUsuario: string
    correo: string
    nombre: string
    fechaAlta: string
    carrera: string
    semestre: number
    usuario: string
    idTipoUsuario: number
  };
  modifica: FormGroup;

  constructor(readonly autoresService: CommonService) {
    this.usuario = {
      idUsuario: sessionStorage?.idUsuario,
      tipoUsuario: sessionStorage?.tipoUsuario,
      correo: sessionStorage?.correo,
      nombre: sessionStorage?.nombre,
      fechaAlta: sessionStorage?.fechaAlta,
      carrera: sessionStorage?.carrera,
      semestre: sessionStorage?.semestre,
      usuario: sessionStorage?.matricula,
      idTipoUsuario: parseInt(sessionStorage?.idTipoUsuario, 10),
    }
    this.modifica = new FormGroup({
      actualPass: new FormControl('', [Validators.required]),
      newPass: new FormControl('', [Validators.required]),
      repeatPass: new FormControl('', [Validators.required])
    });
  }


  actualizarPass() {
    if (this.newPass.value !== this.repeatPass.value) {
      Swal.fire(
        'Error',
        'Las contraseñas no coinciden',
        'info'
      );
    } else {
      Swal.fire({
        title: '¿Deseas continuar con la actualización de la contraseña?',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Actualizar',
        allowEscapeKey: false,
        allowOutsideClick: false,
        allowEnterKey: false,
        preConfirm: () => {
          return this.autoresService.actualizarClave(this.newPass.value, this.actualPass.value, this.usuario.idUsuario).result()
            .then((result: any) => result['data'].actualizarClave)
            .catch(() => {
              Swal.showValidationMessage('No hemos podido actualizar la contraseña');
            });
        },
      }).then((result) => {
        if (result.isConfirmed) {
          if (result.value === -1)
            Swal.fire(
              'Actualiza Autor!',
              'La contraseña anterior no concuerda con nuestros registros',
              'warning'
            );
          else {
            Swal.fire(
              'Completo!',
              'Contraseña actualizada correctamente!',
              'success'
            );
          }
        }
      });
    }
  }

  get actualPass() {
    return this.modifica.controls.actualPass;
  }

  get newPass() {
    return this.modifica.controls.newPass;
  }

  get repeatPass() {
    return this.modifica.controls.repeatPass;
  }
  limpiarFormularioNuevo() {
    this.modifica = new FormGroup({
      actualPass: new FormControl('', [Validators.required]),
      newPass: new FormControl('', [Validators.required]),
      repeatPass: new FormControl('', [Validators.required])
    });

  }
}
