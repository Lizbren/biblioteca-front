import {Component, Inject, OnInit} from '@angular/core';
import {CommonService} from '../../Services/common.service'
import {Prestamo} from '../../interfaces/Prestamos'
import {PageEvent} from "@angular/material/paginator";
import Swal from "sweetalert2";
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material/dialog";
import {ControlPrestamosComponentDialog} from "../control-prestamos/control-prestamos.component";
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import {registerLocaleData} from '@angular/common';
import localeES from "@angular/common/locales/es";

registerLocaleData(localeES, "es");
import {formatDate} from "@angular/common";
import {UES_IMAGE_64, UMB_IMAGE_64} from "../../Constantes/constantes";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-prestamos',
  templateUrl: './prestamos.component.html',
  styleUrls: ['./prestamos.component.scss']
})
export class PrestamosComponent implements OnInit {

  prestamos: Prestamo [] = []
  busqueda: Prestamo [] = []
  breakpoint: any
  length = 0;
  pageSize = 20;
  pageSizeOptions: number[] = [10, 30, 50, 100];
  prestamo: string = '';
  pageEvent: PageEvent = new PageEvent();

  constructor(readonly prestamosService: CommonService, public dialog: MatDialog) {
    this.obtenerPrestamos();
  }

  ngOnInit(): void {
    this.breakpoint = (window.innerWidth <= 400) ? 2 : 3;
  }

  obtenerPrestamos(): void {
    Swal.fire({
      title: 'Obteniedo informacion de los prestamos',
      allowEscapeKey: false,
      allowEnterKey: false,
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      }
    })
    this.prestamosService.obtenerPrestamosPorUsuario(sessionStorage?.idUsuario).result()
      .then((resultados: any) => {
        this.prestamos = resultados.data['prestamosPorUsuario']
        this.busqueda = this.prestamos;
        this.length = this.busqueda.length;
        this.pageEvent.length = this.busqueda.length;
        this.pageEvent.pageSize = this.pageSize;
        this.pageEvent.pageIndex = 0;
        Swal.close();
      })
      .catch(() => {
        Swal.fire(
          'Algo salio mal!',
          'No pudimos obtener la información de los libros',
          'error'
        );
      });
  }

  buscarPrestamos() {
    this.busqueda = [];
    this.prestamos
      .map(value => {
        value.fechaPrestamo ? value.fechaPrestamo : value.fechaPrestamo = '-';
        value.fechaLimite ? value.fechaLimite : value.fechaLimite = '-';
        return value
      })
      .filter(value =>
        (value.titulo.toLowerCase().includes(this.prestamo.toLowerCase())
          || value.fechaApartado.toLowerCase().includes(this.prestamo.toLowerCase())
          || value.fechaPrestamo.toLowerCase().includes(this.prestamo.toLowerCase())
          || value.fechaLimite.toLowerCase().includes(this.prestamo.toLowerCase())
        ))
      .forEach(resultado => this.busqueda.push(resultado));
    this.length = this.busqueda.length;
  }

  renovarPrestamo(idPrestamo: number, titulo: string) {
    Swal.fire({
      title: `El libro ${titulo} sera renovado`,
      showCancelButton: true,
      confirmButtonText: 'Confirmar renovación',
      cancelButtonText: 'Cancelar renovación',
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return this.prestamosService.renovarPrestamo(idPrestamo).result()
          .then(() => 'Se ha renovado la fecha del libro')
          .catch(() => {
            Swal.showValidationMessage(
              `No pudimos actualizar la fecha de renovación`
            )
          });
      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Renovación',
          'Se ha renovado la fecha del libro',
          'success'
        ).then(() => this.obtenerPrestamos());
      }
    });
  }

  cancelarPrestamo(idPrestamo: number) {
    Swal.fire({
      title: `El libro solicitado con el folio ${idPrestamo} sera cancelado`,
      showCancelButton: true,
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar',
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return this.prestamosService.noAutorizarPrestamo(idPrestamo).result()
          .then(() => 'Se ha cancelado')
          .catch(() => {
            Swal.showValidationMessage(
              `No pudimos realizar la operacion`
            )
          });
      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Cancelación',
          'Se ha cancelado la solicitud',
          'success'
        ).then(() => this.obtenerPrestamos());
      }
    });
  }

  openDialog(prestamo: any) {
    this.dialog.open(PrestamosComponentDialog, {
      data: {
        prestamo: prestamo,
      },
    });
  }

  solicitudPDF(prestamo: any) {
    console.log(prestamo)
    const locale = 'en-US';
    const pdfDefinition: any = {
      background: function (page: any) {
        if (page === 1) {
          return [
            {
              table: {
                headerRows: 1,
                heights: [60],
                widths: ['*', '*'],
                body: [
                  [{
                    margin: [0, 20, 0, 0],
                    alignment: 'right',
                    image: UMB_IMAGE_64,
                    fit: [250, 250],
                    border: [false, false, true, false]
                  },
                    {
                      margin: [0, 20, 0, 0],
                      alignment: 'left',
                      image: UES_IMAGE_64,
                      fit: [250, 250],
                      border: [false, false, false, false]
                    }]
                ]
              },
              margin: [0, 20],
            }
          ];
        }
        return [];
      },

      content: [
        '\n\n\n\n\n\n\n\n\n',
        {
          text: 'SOLICITUD DE PRÉSTAMO PARA MATERIAL BIBLIOGRAFICO',
          style: 'header',
          alignment: 'center'
        },
        {
          text: 'Datos del alumno',
          style: 'subheader',
          alignment: 'center'
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [{text: 'Nombre completo:', alignment: 'right'}, prestamo.nombre],
              [{text: 'Matricula:', alignment: 'right'}, prestamo.matricula],
              [{text: 'Carrera:', alignment: 'right'}, prestamo.carrera],
              [{text: 'Semestre:', alignment: 'right'}, prestamo.semestre],
              [{text: 'Folio del prestamo:', alignment: 'right'}, prestamo.idPrestamo]
            ]
          },
          layout: 'noBorders'
        },
        {
          text: 'Detalle de prestamo',
          style: 'subheader',
          alignment: 'center'
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [{text: 'Detalle del libro', alignment: 'center'}, {text: 'Detalle de fechas', alignment: 'center'}],
              [{
                style: 'tableExample',
                table: {
                  widths: ['*', '*'],
                  body: [
                    [{text: 'Titulo:', alignment: 'right'}, prestamo.titulo],
                    [{text: 'ISBN:', alignment: 'right'}, prestamo.isbn],
                    [{text: 'Edicion:', alignment: 'right'}, prestamo.edicion],
                    [{text: 'Editorial:', alignment: 'right'}, prestamo.editorial],
                  ]
                },
                layout: 'noBorders'
              },
                {
                  style: 'tableExample',
                  table: {

                    body: [
                      [{
                        text: 'Fecha de apartado:',
                        alignment: 'right'
                      }, formatDate(prestamo.fechaApartado, 'yyyy-mm-dd HH:mm', locale)]
                    ]
                  },
                  layout: 'noBorders'
                },]
            ]
          }
        },
        {
          stack: [
            'Estimado usuario para poder recoger tu material bibliográfico deberás tomar en cuenta lo siguiente:',
            '1.	Mostrar la solicitud al encargado de la biblioteca. ',
            '2.	Tienes un plazo de 24 horas para recoger tu libro posterior a realizar la solicitud de lo contrario la solicitud será cancelada.',
            '3.	Recuerda presentar tu identificación escolar vigente. ',
            '4.	El préstamo será de tres días naturales a partir de la fecha en que se solicitan. ',
            '5.	Podrás renovar el préstamo únicamente por una vez dentro del plazo marcado, de lo contrario una vez entregado o finalizado el préstamo deberás volver a solicitarlo. ',
            '6.	Cuando un libro sea devuelto con posteridad a la fecha de vencimiento del préstamo la biblioteca cobrará una multa por libro y por día de retraso, equivalente $17 MXM ',
            '7.	A partir de diez días de retraso de la fecha de vencimiento se le cancelará su derecho a préstamo por 1 mes y si no se presenta físicamente el libro se declarará perdido o dañado y el lector deberá pagar su valor o reponerlo, además de pagar la multa correspondiente'
          ],
          alignment: 'justify'
        }
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10]
        },
        subheader: {
          fontSize: 15,
          bold: true
        },
        tableExample: {
          margin: [0, 5, 0, 15]
        },
      }
    }

    const pdf = pdfMake.createPdf(pdfDefinition);
    pdf.open();

  }
}

@Component({
  selector: 'detalle-prestamo-alumno',
  templateUrl: 'detalle-prestamo-alumno.html',
})
export class PrestamosComponentDialog {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }
}
