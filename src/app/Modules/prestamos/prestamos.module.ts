import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrestamosRoutingModule } from './prestamos-routing.module';
import {PrestamosComponent, PrestamosComponentDialog} from './prestamos.component';

import { SharedModule } from '../shared.module';

@NgModule({
  declarations: [
    PrestamosComponent, PrestamosComponentDialog
  ],
  imports: [
    CommonModule,
    PrestamosRoutingModule,
    SharedModule
  ]
})
export class PrestamosModule { }
