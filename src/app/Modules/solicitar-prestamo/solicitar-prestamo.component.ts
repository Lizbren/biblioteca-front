import {Component, Inject} from '@angular/core';
import {CommonService} from "../../Services/common.service";
import {Libro} from "../../interfaces/Libro";
import Swal from 'sweetalert2';
import {PageEvent} from "@angular/material/paginator";
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material/dialog";


@Component({
  selector: 'app-solicitar-prestamo',
  templateUrl: './solicitar-prestamo.component.html',
  styleUrls: ['./solicitar-prestamo.component.scss']
})
export class SolicitarPrestamoComponent {
  libros: Libro[] = [];
  busqueda: Libro[] = [];
  libro: string = '';
  length = 0;
  pageSize = 20;
  pageSizeOptions: number[] = [10, 30, 50, 100];

  pageEvent: PageEvent = new PageEvent();

  constructor(readonly libroService: CommonService, public dialog: MatDialog) {
    this.obtenerLibros();
  }

  obtenerLibros() {
    this.libroService.obtenerLibros().result()
      .then((resultados: any) => {
        this.libros = resultados.data['libros'];
        this.busqueda = this.libros;
        this.length = this.busqueda.length;
        this.pageEvent.length = this.busqueda.length;
        this.pageEvent.pageSize = this.pageSize;
        this.pageEvent.pageIndex = 0;
      })
      .catch(error => console.log(error));
  }

  buscarLibros() {
    this.busqueda = [];
    this.libros
      .filter(value =>
        (value.titulo.toLowerCase().includes(this.libro.toLowerCase())
          || value.autores.join(',').toLowerCase().includes(this.libro.toLowerCase())
          || value.categorias.join(',').toLowerCase().includes(this.libro.toLowerCase())
          || value.isbn.toLowerCase().includes(this.libro.toLowerCase())
        )
      )
      .forEach(resultado => this.busqueda.push(resultado));
    this.length = this.busqueda.length;
  }

  solicitarPrestamo(libro: any) {
    let solicitud = {
      idUsuario: sessionStorage?.idUsuario,
      idLibro: libro.idLibro,
      vencido: false,
      cuota: 0.0,
      autorizado: false,
      renovaciones: 0
    }
    this.libroService.solicitarPrestamo(solicitud).result()
      .then((solicitud: any) => {
        console.log(solicitud.data['solicitarPrestamo'])
        if (solicitud.data['solicitarPrestamo'] === -1)
          Swal.fire(
            'Solicitud', `No se permite solicitar más de dos prestamos`, 'warning'
          );
        else if (solicitud.data['solicitarPrestamo'] === -2)
          Swal.fire(
            'Solicitud', `Ya solicitaste este libro`, 'warning'
          );
        else
          Swal.fire(
            `Solcitud Enviada con folio: ${solicitud.data['solicitarPrestamo']}`,
            'Imprime tu solicitud en la seccion de Prestamos',
            'success'
          ).then(() => {
            this.obtenerLibros();
          })
      })
      .catch(() => {
        Swal.fire(
          'Solcitud no enviada', `Hubo un error al solicitar tu prestamo o y ano hay libros disponibles, intenta recargar la pagina.`, 'error'
        )
      });
  }

  openDialog(libro: any) {
    this.dialog.open(SolicitarPrestamoComponentDialog, {
      data: {
        libro: libro,
      },
    });
  }


}

@Component({
  selector: 'detalle-libro-alumno',
  templateUrl: 'detalle-libro-alumno.html',
})
export class SolicitarPrestamoComponentDialog {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }
}
