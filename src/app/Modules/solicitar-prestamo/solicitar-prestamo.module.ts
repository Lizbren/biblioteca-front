import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SolicitarPrestamoRoutingModule} from './solicitar-prestamo-routing.module';
import {SolicitarPrestamoComponent, SolicitarPrestamoComponentDialog} from './solicitar-prestamo.component';

import {SharedModule} from '../shared.module';

@NgModule({
  declarations: [
    SolicitarPrestamoComponent, SolicitarPrestamoComponentDialog
  ],
  imports: [
    CommonModule,
    SolicitarPrestamoRoutingModule,
    SharedModule
  ]
})
export class SolicitarPrestamoModule {
}
