import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ControlPrestamosComponent } from './control-prestamos.component';

describe('CategoriasComponent', () => {
  let component: ControlPrestamosComponent;
  let fixture: ComponentFixture<ControlPrestamosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ControlPrestamosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlPrestamosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
