import {Component, Inject} from '@angular/core';
import {CommonService} from '../../Services/common.service'
import Swal from 'sweetalert2';
import {Prestamo} from '../../interfaces/Prestamos'
import {PageEvent} from "@angular/material/paginator";
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material/dialog";
import {UES_IMAGE_64, UMB_IMAGE_64} from "../../Constantes/constantes";
import {formatDate} from "@angular/common";
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-control-prestamos',
  templateUrl: './control-prestamos.component.html',
  styleUrls: ['./control-prestamos.component.scss']
})
export class ControlPrestamosComponent {
  prestamos: Prestamo[] = [];
  busqueda: Prestamo[] = [];
  filtro = '1'
  select = [
    {nombre: 'autorizado', value: true, vencido: false},
    {nombre: 'autorizado', value: false, vencido: false},
    {nombre: 'autorizado', value: true, vencido: true},
    {nombre: 'autorizado', value: false, vencido: true},]

  matricula: string = '';
  folio: string = '';
  length = 0;
  pageSize = 20;
  pageSizeOptions: number[] = [10, 30, 50, 100];
  pageEvent: PageEvent = new PageEvent();

  constructor(readonly controlPrestamosService: CommonService, public dialog: MatDialog) {
    this.obtenerPrestamos(true);
  }

  openDialog(prestamo: any) {
    this.dialog.open(ControlPrestamosComponentDialog, {
      data: {
        prestamo: prestamo,
      },
    });
  }

  obtenerPrestamos(alerta: boolean) {
    if (alerta) {
      Swal.fire({
        title: 'Obteniedo informacion de los prestamos',
        allowEscapeKey: false,
        allowEnterKey: false,
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        }
      });
    }
    this.controlPrestamosService.obtenerPrestamos().result()
      .then((prestamos: any) => {
        this.prestamos = prestamos.data['prestamos'];
        this.busqueda = this.prestamos;
        this.length = this.busqueda.length;
        this.pageEvent.length = this.busqueda.length;
        this.pageEvent.pageSize = this.pageSize;
        this.pageEvent.pageIndex = 0;
        this.length = this.busqueda.length;
        if (alerta) Swal.close()
        if (this.prestamos.length > 0)
          this.buscarPrestamo(alerta);
      }).catch(() => {
      Swal.fire(
        'Fallo!',
        'No hemos podido obtener información sobre los categorias!',
        'error'
      );
    });
  }

  buscarPrestamo(alerta: boolean) {
    if (alerta) {
      Swal.fire({
        title: 'Buscando Prestamo',
        didOpen: () => {
          Swal.showLoading();
        }
      });
    }

    this.busqueda = [];

    if (this.matricula !== '') {
      this.byType('matricula', this.matricula)
    } else if (this.folio !== '') {
      this.byType('idPrestamo', this.folio.toString())
    } else this.onlySelect();
    if (alerta) Swal.close();
    this.pageEvent.length = this.busqueda.length;
    this.pageEvent.pageSize = this.pageSize;
    this.pageEvent.pageIndex = 0;
    this.length = this.busqueda.length;
  }

  byType(name: string, value: string) {
    let temp = this.select[parseInt(this.filtro, 10)]
    this.prestamos.filter((filtrados: any) => {
        return filtrados[temp.nombre] === temp.value && filtrados.vencido === temp.vencido &&
          (filtrados[name].toString().includes(value))
      }
    ).forEach(resultado => {
      this.busqueda.push(resultado)
    });
  }

  onlySelect() {
    let temp = this.select[parseInt(this.filtro, 10)]
    this.prestamos.filter((filtrados: any) => filtrados[temp.nombre] === temp.value && filtrados.vencido === temp.vencido
    ).forEach(resultado => {
      this.busqueda.push(resultado)
    });
    this.length = this.busqueda.length;
  }

  autorizarPrestamo(idPrestamo: number) {
    let idUsuario = sessionStorage?.idUsuario
    this.controlPrestamosService.autorizarPrestamo(idPrestamo, idUsuario).result()
      .then(() => {
        Swal.fire(
          'Autorizado',
          'Prestamo autorizado con exito',
          'success'
        );
      })
      .then(() => {
        this.obtenerPrestamos(false);
      })
      .catch(() => {
        Swal.fire(
          'Fallo!',
          'No se pudo autorizar el prestamo!',
          'error'
        );
      });
  }

  actualizarDeudas() {
    this.controlPrestamosService.actualizarDeudas().result()
      .then(() => {
        Swal.fire(
          'Actualizado',
          'Deudas Actualizadas con exito',
          'success'
        );
      })
      .then(() => {
        this.obtenerPrestamos(false);
      })
      .catch(() => {
        Swal.fire(
          'Fallo!',
          'No se pudieron actualizar las deudas!',
          'error'
        );
      });
  }

  actualizarVencimiento() {
    this.controlPrestamosService.actualizarVencimiento().result()
      .then(() => {
        Swal.fire(
          'Actualizado',
          'Prestamos vencidos actualizados con exito',
          'success'
        );
      })
      .then(() => {
        this.obtenerPrestamos(false);
      })
      .catch(() => {
        Swal.fire(
          'Fallo!',
          'No se pudieron actualizar los prestamos!',
          'error'
        );
      });
  }

  finalizarPrestamo(prestamo: any) {
    this.controlPrestamosService.finalizarPrestamo(prestamo.idPrestamo).result()
      .then(() => {
        Swal.fire(
          'Entregado',
          'Libro devuelto a la biblioteca',
          'success'
        );
      })
      .then(v => {
        this.obtenerPrestamos(false);
      })
      .catch(() => {
        Swal.fire(
          'Fallo!',
          'No pudimos devolver el libro',
          'error'
        );
      });
  }

  buscarPrestamoFolio(alerta: boolean) {
    if (alerta) {
      Swal.fire({
        title: 'Buscando Prestamo',
        didOpen: () => {
          Swal.showLoading();
        }
      });
    }

    this.busqueda = [];
    this.matricula = ''
    if (this.folio === '') {
      this.onlySelect();
    } else {

    }
    if (alerta) Swal.close();
    this.pageEvent.length = this.busqueda.length;
    this.pageEvent.pageSize = this.pageSize;
    this.pageEvent.pageIndex = 0;
    this.length = this.busqueda.length;
  }

  noAutorizar(idPrestamo: number) {
    Swal.fire({
      title: `El libro solicitado con el folio ${idPrestamo} no se autorizara`,
      showCancelButton: true,
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar',
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return this.controlPrestamosService.noAutorizarPrestamo(idPrestamo).result()
          .then(() => true)
          .catch(() => {
            Swal.showValidationMessage(
              `No pudimos realizar la operacion`
            )
          });
      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      if (result.isConfirmed) {
        console.log("fino");
        Swal.fire(
          'Cancelación',
          'La solicitud no fue autorizada',
          'success'
        ).then(() => this.obtenerPrestamos(true));
      }
    });
  }

  autorizacionPDF(prestamo: any) {
    const locale = 'en-US';
    const pdfDefinition: any = {
      background: function (page: any) {
        if (page === 1) {
          return [
            {
              table: {
                headerRows: 1,
                heights: [60],
                widths: ['*', '*'],
                body: [
                  [{
                    margin: [0, 20, 0, 0],
                    alignment: 'right',
                    image: UMB_IMAGE_64,
                    fit: [250, 250],
                    border: [false, false, true, false]
                  },
                    {
                      margin: [0, 20, 0, 0],
                      alignment: 'left',
                      image: UES_IMAGE_64,
                      fit: [250, 250],
                      border: [false, false, false, false]
                    }]
                ]
              },
              margin: [0, 20],
            }
          ];
        }
        return [];
      },

      content: [
        '\n\n\n\n\n\n\n\n\n',
        {
          text: 'AUTORIZACIÓN DE PRÉSTAMO PARA MATERIAL BIBLIOGRAFICO',
          style: 'header',
          alignment: 'center'
        },
        {
          text: 'Datos del alumno',
          style: 'subheader',
          alignment: 'center'
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [{text: 'Nombre completo:', alignment: 'right'}, prestamo.nombre],
              [{text: 'Matricula:', alignment: 'right'}, prestamo.matricula],
              [{text: 'Carrera:', alignment: 'right'}, prestamo.carrera],
              [{text: 'Semestre:', alignment: 'right'}, prestamo.semestre]
            ]
          },
          layout: 'noBorders'
        },
        {
          text: 'Detalle del administrador',
          style: 'subheader',
          alignment: 'center'
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [{text: 'Nombre del administrador:', alignment: 'right'}, prestamo.nombreAdministrador]]
          },
          layout: 'noBorders'
        },
        {
          text: 'Detalle de prestamo',
          style: 'subheader',
          alignment: 'center'
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [{text: 'Folio:', alignment: 'right'}, prestamo.idPrestamo],
              [{
                text: 'Fecha entrega o renovación:',
                alignment: 'right'
              }, formatDate(prestamo.fechaLimite, 'yyyy-MM-dd HH:mm', locale)]
            ]
          },
          layout: 'noBorders'
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [{text: 'Detalle del libro', alignment: 'center'}, {text: 'Detalle de fechas', alignment: 'center'}],
              [{
                style: 'tableExample',
                table: {
                  widths: ['*', '*'],
                  body: [
                    [{text: 'Titulo:', alignment: 'right'}, prestamo.titulo],
                    [{text: 'ISBN:', alignment: 'right'}, prestamo.isbn],
                    [{text: 'Edicion:', alignment: 'right'}, prestamo.edicion],
                    [{text: 'Editorial:', alignment: 'right'}, prestamo.editorial],
                  ]
                },
                layout: 'noBorders'
              },
                {
                  style: 'tableExample',
                  table: {
                    body: [
                      [{
                        text: 'Fecha de apartado:',
                        alignment: 'right'
                      }, formatDate(prestamo.fechaApartado, 'yyyy-MM-dd HH:mm', locale)],
                      [{
                        text: 'Fecha de prestamo:',
                        alignment: 'right'
                      }, formatDate(prestamo.fechaPrestamo, 'yyyy-MM-dd HH:mm', locale)]
                    ]
                  },
                  layout: 'noBorders'
                },]
            ]
          }
        },
        {
          stack: [
            'Estimado usuario para poder recoger tu material bibliográfico deberás tomar en cuenta lo siguiente:',
            '1.	Mostrar la solicitud al encargado de la biblioteca. ',
            '2.	Tienes un plazo de 24 horas para recoger tu libro posterior a realizar la solicitud de lo contrario la solicitud será cancelada.',
            '3.	Recuerda presentar tu identificación escolar vigente. ',
            '4.	El préstamo será de tres días naturales a partir de la fecha en que se solicitan. ',
            '5.	Podrás renovar el préstamo únicamente por una vez dentro del plazo marcado, de lo contrario una vez entregado o finalizado el préstamo deberás volver a solicitarlo. ',
            '6.	Cuando un libro sea devuelto con posteridad a la fecha de vencimiento del préstamo la biblioteca cobrará una multa por libro y por día de retraso, equivalente $17 MXM ',
            '7.	A partir de diez días de retraso de la fecha de vencimiento se le cancelará su derecho a préstamo por 1 mes y si no se presenta físicamente el libro se declarará perdido o dañado y el lector deberá pagar su valor o reponerlo, además de pagar la multa correspondiente'
          ],
          alignment: 'justify'
        }
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10]
        },
        subheader: {
          fontSize: 15,
          bold: true
        },
        tableExample: {
          margin: [0, 5, 0, 15]
        },
      }
    }

    const pdf = pdfMake.createPdf(pdfDefinition);
    pdf.open();

  }

  entregaPDF(prestamo: any) {
    const locale = 'en-US';
    const pdfDefinition: any = {
      background: function (page: any) {
        if (page === 1) {
          return [
            {
              table: {
                headerRows: 1,
                heights: [60],
                widths: ['*', '*'],
                body: [
                  [{
                    margin: [0, 20, 0, 0],
                    alignment: 'right',
                    image: UMB_IMAGE_64,
                    fit: [250, 250],
                    border: [false, false, true, false]
                  },
                    {
                      margin: [0, 20, 0, 0],
                      alignment: 'left',
                      image: UES_IMAGE_64,
                      fit: [250, 250],
                      border: [false, false, false, false]
                    }]
                ]
              },
              margin: [0, 20],
            }
          ];
        }
        return [];
      },

      content: [
        '\n\n\n\n\n\n\n\n\n',
        {
          text: 'ACUSE DE FINALIZACIÓN DE PRESTAMO PARA MATERIAL BIBLIOGRAFICO',
          style: 'header',
          alignment: 'center'
        },
        {
          text: 'Datos del alumno',
          style: 'subheader',
          alignment: 'center'
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [{text: 'Nombre completo:', alignment: 'right'}, prestamo.nombre],
              [{text: 'Matricula:', alignment: 'right'}, prestamo.matricula],
              [{text: 'Carrera:', alignment: 'right'}, prestamo.carrera],
              [{text: 'Semestre:', alignment: 'right'}, prestamo.semestre]
            ]
          },
          layout: 'noBorders'
        },
        {
          text: 'Administrador',
          style: 'subheader',
          alignment: 'center'
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [{text: 'Nombre del administrador:', alignment: 'right'}, prestamo.nombreAdministrador]]
          },
          layout: 'noBorders'
        },
        {
          text: 'Detalle de prestamo',
          style: 'subheader',
          alignment: 'center'
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [{text: 'Folio:', alignment: 'right'}, prestamo.idPrestamo],
              [{text: 'Multa:', alignment: 'right'}, '$' + prestamo.cuota],
              [{text: 'Renovaciones:', alignment: 'right'}, prestamo.renovaciones],
              [{text: 'Fecha entrega:', alignment: 'right'}, formatDate(prestamo.fechaDevolucion, 'yyyy-MM-dd HH:mm', locale)]
            ]
          },
          layout: 'noBorders'
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [{text: 'Detalle del libro', alignment: 'center'}, {text: 'Detalle de fechas', alignment: 'center'}],
              [{
                style: 'tableExample',
                table: {
                  widths: ['*', '*'],
                  body: [
                    [{text: 'Titulo:', alignment: 'right'}, prestamo.titulo],
                    [{text: 'ISBN:', alignment: 'right'}, prestamo.isbn],
                    [{text: 'Edicion:', alignment: 'right'}, prestamo.edicion],
                    [{text: 'Editorial:', alignment: 'right'}, prestamo.editorial],
                  ]
                },
                layout: 'noBorders'
              },
                {
                  style: 'tableExample',
                  table: {
                    body: [
                      [{
                        text: 'Fecha de apartado:',
                        alignment: 'right'
                      }, formatDate(prestamo.fechaApartado, 'yyyy-MM-dd HH:mm', locale)],
                      [{
                        text: 'Fecha de prestamo:',
                        alignment: 'right'
                      }, formatDate(prestamo.fechaPrestamo, 'yyyy-MM-dd HH:mm', locale)]
                    ]
                  },
                  layout: 'noBorders'
                },]
            ]
          }
        },
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10]
        },
        subheader: {
          fontSize: 15,
          bold: true
        },
        tableExample: {
          margin: [0, 5, 0, 15]
        },
      }
    }

    const pdf = pdfMake.createPdf(pdfDefinition);
    pdf.open();

  }
}

@Component({
  selector: 'card-detalle-prestamo',
  templateUrl: 'card-detalle-prestamo.html',
})
export class ControlPrestamosComponentDialog {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }
}
