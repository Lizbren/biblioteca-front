import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ControlPrestamosComponent} from './control-prestamos.component'
const routes: Routes = [
  { path: '', component: ControlPrestamosComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ControlPrestamosRoutingModule { }
