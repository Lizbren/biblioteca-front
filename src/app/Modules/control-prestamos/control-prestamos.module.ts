import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ControlPrestamosRoutingModule } from './control-prestamos-routing.module';
import {ControlPrestamosComponent, ControlPrestamosComponentDialog} from './control-prestamos.component';

import { SharedModule } from '../shared.module';

@NgModule({
  declarations: [
    ControlPrestamosComponent, ControlPrestamosComponentDialog
  ],
  imports: [
    CommonModule,
    ControlPrestamosRoutingModule,
    SharedModule
  ]
})
export class ControlPrestamosModule { }
