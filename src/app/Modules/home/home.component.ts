import {Component} from '@angular/core';
import {CommonService} from "../../Services/common.service";
import Swal from "sweetalert2";
import {Prestamo} from "../../interfaces/Prestamos";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  slides = [
    {'image': 'assets/img1.png'},
    {'image': 'assets/img2.png'},
    {'image': 'assets/img3.png'},
    {'image': 'assets/img4.png'},
  ];
  prestamos: Prestamo[] = [];
  deuda: number = 0;
  tipoUsuario: string = '';

  constructor(readonly controlPrestamosService: CommonService) {
    this.tipoUsuario = sessionStorage?.tipoUsuario;
    if (this.tipoUsuario === 'ALUMNO') {
      Swal.fire({
        title: 'Obteniedo informacion de los prestamos',
        allowEscapeKey: false,
        allowEnterKey: false,
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        }
      });
      let idUsuario = sessionStorage?.idUsuario;
      this.controlPrestamosService.actualizarDeudaPorUsuario(idUsuario).result()
        .then(() => controlPrestamosService.actualizarVencimientoPorUsuario(idUsuario).result())
        .then(() => controlPrestamosService.obtenerPrestamosPorUsuario(idUsuario).result())
        .then((prestamos: any) => {
          this.prestamos = prestamos.data['prestamosPorUsuario'];
          this.prestamos.filter(elemento => elemento.autorizado && !elemento.vencido)
            .forEach(deuda => this.deuda = deuda.cuota + this.deuda);
        })
        .then(() => {
          Swal.fire(
            'Validando',
            'Hemos actualizado la información de tus prestamos.',
            'success'
          );
        })
        .catch(() => {
          Swal.fire(
            'Validando',
            'No hemos podido actualizar la información de tus prestamos.',
            'error'
          );
        })
    }
  }
}
