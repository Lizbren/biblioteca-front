import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibrosRoutingModule } from './libros-routing.module';
import {LibrosComponent, LibrosComponentDialog} from './libros.component';

import { SharedModule } from '../shared.module';

@NgModule({
  declarations: [
    LibrosComponent, LibrosComponentDialog
  ],
  imports: [
    CommonModule,
    LibrosRoutingModule,
    SharedModule,
  ]
})
export class LibrosModule { }
