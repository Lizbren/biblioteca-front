import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CommonService} from '../../Services/common.service'
import Swal from 'sweetalert2';
import {PageEvent} from "@angular/material/paginator";
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material/dialog";


@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.scss']
})
export class LibrosComponent implements OnInit {
  selectedLibro: any = {
    idLibro: 0,
    titulo: '',
    isbn: '',
    paginas: 0,
    existencia: 0,
    edicion: 0,
    editorial: 0,
    idEditorial: 0,
    categorias: 0,
    idCategorias: 0,
    autores: 0,
    idAutores: 0,
    disponibles: 0,
    prestados: 0,
    apartados: 0
  }
  categoriasVigentes: Array<any> = [];
  autoresVigentes: Array<any> = [];
  editorialesVigentes: Array<any> = [];
  libros: Array<any> = [];
  nombresFiltrados = this.libros.slice();
  nuevoLibroForm: FormGroup;
  modificaLibroForm: FormGroup;
  buscar: string = '';
  busqueda: Array<any> = [];
  length = 0;
  pageSize = 20;
  pageSizeOptions: number[] = [10, 20, 30, 50, 100];
  pageEvent: PageEvent = new PageEvent();
  dataSource: any;

  constructor(readonly libroService: CommonService, public dialog: MatDialog) {
    this.nuevoLibroForm = new FormGroup({
      titulo: new FormControl('', [Validators.required]),
      isbn: new FormControl('', [Validators.required]),
      paginas: new FormControl('', [Validators.required]),
      existencia: new FormControl('', [Validators.required]),
      edicion: new FormControl('', [Validators.required]),
      editorial: new FormControl('', [Validators.required]),
      categorias: new FormControl('', [Validators.required]),
      autores: new FormControl('', [Validators.required]),
      disponibles: new FormControl({value: 0, disabled: true}, [Validators.required]),
      prestados: new FormControl({value: 0, disabled: true}, [Validators.required]),
      apartados: new FormControl({value: 0, disabled: true}, [Validators.required])
    });
    this.modificaLibroForm = new FormGroup({
      mIdLibro: new FormControl({value: '', disabled: true}, [Validators.required]),
      mTitulo: new FormControl('', [Validators.required]),
      mIsbn: new FormControl('', [Validators.required]),
      mPaginas: new FormControl('', [Validators.required]),
      mExistencia: new FormControl('', [Validators.required]),
      mEdicion: new FormControl('', [Validators.required]),
      mEditorial: new FormControl('', [Validators.required]),
      mCategorias: new FormControl('', [Validators.required]),
      mAutores: new FormControl('', [Validators.required]),
      mDisponibles: new FormControl('', [Validators.required]),
      mPrestados: new FormControl('', [Validators.required]),
      mApartados: new FormControl('', [Validators.required])
    });
  }

  openDialog(libro: any) {
    this.dialog.open(LibrosComponentDialog, {
      data: {
        libro: libro,
      },
    });
  }

  get titulo() {
    return this.nuevoLibroForm.controls.titulo;
  }

  get isbn() {
    return this.nuevoLibroForm.controls.isbn;
  }

  get paginas() {
    return this.nuevoLibroForm.controls.paginas;
  }

  get existencia() {
    return this.nuevoLibroForm.controls.existencia;
  }

  get edicion() {
    return this.nuevoLibroForm.controls.edicion;
  }

  get editorial() {
    return this.nuevoLibroForm.controls.editorial;
  }

  get categorias() {
    return this.nuevoLibroForm.controls.categorias;
  }

  get autores() {
    return this.nuevoLibroForm.controls.autores;
  }

  get disponibles() {
    return this.nuevoLibroForm.controls.disponibles;
  }

  get prestados() {
    return this.nuevoLibroForm.controls.prestados;
  }

  get apartados() {
    return this.nuevoLibroForm.controls.apartados;
  }

  get mIdLibro() {
    return this.modificaLibroForm.controls.mIdLibro;
  }

  get mTitulo() {
    return this.modificaLibroForm.controls.mTitulo;
  }

  get mIsbn() {
    return this.modificaLibroForm.controls.mIsbn;
  }

  get mPaginas() {
    return this.modificaLibroForm.controls.mPaginas;
  }

  get mExistencia() {
    return this.modificaLibroForm.controls.mExistencia;
  }

  get mEdicion() {
    return this.modificaLibroForm.controls.mEdicion;
  }

  get mEditorial() {
    return this.modificaLibroForm.controls.mEditorial;
  }

  get mCategorias() {
    return this.modificaLibroForm.controls.mCategorias;
  }

  get mAutores() {
    return this.modificaLibroForm.controls.mAutores;
  }

  get mDisponibles() {
    return this.modificaLibroForm.controls.mDisponibles;
  }

  get mPrestados() {
    return this.modificaLibroForm.controls.mPrestados;
  }

  get mApartados() {
    return this.modificaLibroForm.controls.mApartados;
  }

  ngOnInit(): void {
    this.obtenerCategorias();
    this.obtenerAutores();
    this.obtenerEditoriales();
    this.obtenerLibros();
  }

  onChange() {
    this.modificaLibroForm.controls.mIdLibro.setValue(this.selectedLibro.idLibro);
    this.modificaLibroForm.controls.mTitulo.setValue(this.selectedLibro.titulo);
    this.modificaLibroForm.controls.mIsbn.setValue(this.selectedLibro.isbn);
    this.modificaLibroForm.controls.mPaginas.setValue(this.selectedLibro.paginas);
    this.modificaLibroForm.controls.mExistencia.setValue(this.selectedLibro.existencia);
    this.modificaLibroForm.controls.mEdicion.setValue(this.selectedLibro.edicion);
    this.modificaLibroForm.controls.mEditorial.setValue(this.selectedLibro.idEditorial);
    this.modificaLibroForm.controls.mCategorias.setValue(this.selectedLibro.idCategorias);
    this.modificaLibroForm.controls.mAutores.setValue(this.selectedLibro.idAutores);
    this.modificaLibroForm.controls.mDisponibles.setValue(this.selectedLibro.disponibles);
    this.modificaLibroForm.controls.mPrestados.setValue(this.selectedLibro.prestados);
    this.modificaLibroForm.controls.mApartados.setValue(this.selectedLibro.apartados);
  }

  nuevoLibro(): void {
    const libro = {
      titulo: this.titulo.value,
      isbn: this.isbn.value,
      paginas: this.paginas.value,
      existencia: this.existencia.value,
      edicion: this.edicion.value,
      editorial: this.editorial.value,
      categorias: this.categorias.value,
      autores: this.autores.value,
      disponibles: this.existencia.value,
      prestados: 0,
      apartados: 0
    }
    this.libroService.nuevoLibro(libro).result()
      .then((nuevoLibro: any) => {
        if (nuevoLibro['data'].nuevoLibro === -1)
          Swal.fire(
            'Nuevo Libro!',
            'Al parecer este libro ya esta registrado!',
            'warning'
          ).then(() => this.obtenerLibros());
        else {
          this.limpiarFormularioNuevo();
          Swal.fire(
            'Completo!',
            'Libro registrado correctamente!',
            'success'
          ).then(() => this.obtenerLibros());
        }
      })
      .catch(() => {
        Swal.fire(
          'Fallo!',
          'No hemos podido registrar el libro!',
          'error'
        );
      });
  }

  obtenerCategorias() {
    this.libroService.obtenerCategoria().result()
      .then((categorias: any) => {
        categorias.data['categorias'].forEach((categoria: any) => {
          let temp: any = {...categoria};
          if (temp.vigente)
            this.categoriasVigentes.push(temp);
        });
      }).catch(() => {
      Swal.fire(
        'Fallo!',
        'No hemos podido obtener información sobre las categorias!',
        'error'
      );
    });
  }

  obtenerAutores() {
    this.libroService.obtenerAutores().result()
      .then((autores: any) => {
        autores.data['autores'].forEach((autor: any) => {
          let temp: any = {...autor};
          if (temp.vigente)
            this.autoresVigentes.push(temp);
        });
      }).catch(() => {
      Swal.fire(
        'Fallo!',
        'No hemos podido obtener información sobre los autores!',
        'error'
      );
    });
  }

  obtenerEditoriales() {
    this.libroService.obtenerEditorial().result()
      .then((editoriales: any) => {
        editoriales.data['editoriales'].forEach((autor: any) => {
          let temp: any = {...autor};
          if (temp.vigente)
            this.editorialesVigentes.push(temp);
        });
      }).catch(() => {
      Swal.fire(
        'Fallo!',
        'No hemos podido obtener información sobre los autores!',
        'error'
      );
    });
  }

  obtenerLibros() {
    this.libroService.obtenerLibros().result()
      .then((libros: any) => {
        this.libros = libros.data['libros'];
        this.nombresFiltrados = this.libros.slice();
        this.busqueda = this.libros;
        this.pageEvent.length = this.busqueda.length;
        this.pageEvent.pageSize = this.pageSize;
        this.pageEvent.pageIndex = 0;
        this.length = this.busqueda.length;
      }).catch(() => {
      Swal.fire(
        'Fallo!',
        'No hemos podido obtener información sobre los libros!',
        'error'
      );
    });
  }

  actualizarLibro(): void {
    const libro = {
      titulo: this.mTitulo.value,
      isbn: this.mIsbn.value,
      paginas: this.mPaginas.value,
      existencia: this.mExistencia.value,
      edicion: this.mEdicion.value,
      editorial: this.mEditorial.value,
      categorias: this.mCategorias.value,
      autores: this.mAutores.value,
      disponibles: this.mExistencia.value,
      prestados: this.mPrestados.value,
      apartados: this.mApartados.value
    }
    Swal.fire({
      title: '¿Deseas continuar con la actualización?',
      showLoaderOnConfirm: true,
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Actualizar',
      allowEscapeKey: false,
      allowOutsideClick: false,
      allowEnterKey: false,
      preConfirm: () => {
        return this.libroService.actualizarLibro(libro, this.mIdLibro.value).result()
          .then((result: any) => result['data'].actualizaLibro)
          .catch(() => {
            Swal.showValidationMessage('No hemos podido actualiza la categoria');
          });
      },
    }).then((result) => {
      if (result.isConfirmed) {
        if (result.value === -1)
          Swal.fire(
            'Actualiza Libro!',
            'Los datos para actualizar son iguales a un libro ya registrado!!',
            'warning'
          ).then(() => this.obtenerLibros());
        else {
          this.limpiarFormularioActualizar();
          Swal.fire(
            'Completo!',
            'Libro actualizado correctamente!',
            'success'
          ).then(() => this.obtenerLibros());
        }
      }
    });
  }

  buscarLibros() {
    this.busqueda = [];

    if (this.buscar === '')
      this.busqueda = this.libros;
    else
      this.libros
        .filter(value =>
          (value.titulo.toLowerCase().includes(this.buscar.toLowerCase())
            || value.autores.join(',').toLowerCase().includes(this.buscar.toLowerCase())
            || value.categorias.join(',').toLowerCase().includes(this.buscar.toLowerCase())
            || value.isbn.toLowerCase().includes(this.buscar.toLowerCase())
            || value.editorial.toLowerCase().includes(this.buscar.toLowerCase())
          )
        )
        .forEach(resultado => this.busqueda.push(resultado));

    this.pageEvent.length = this.busqueda.length;
    this.pageEvent.pageSize = this.pageSize;
    this.pageEvent.pageIndex = 0;
    this.length = this.busqueda.length;
  }

  limpiarFormularioActualizar() {
    this.modificaLibroForm = new FormGroup({
      mIdLibro: new FormControl({value: '', disabled: true}, [Validators.required]),
      mTitulo: new FormControl('', [Validators.required]),
      mIsbn: new FormControl('', [Validators.required]),
      mPaginas: new FormControl('', [Validators.required]),
      mExistencia: new FormControl('', [Validators.required]),
      mEdicion: new FormControl('', [Validators.required]),
      mEditorial: new FormControl('', [Validators.required]),
      mCategorias: new FormControl('', [Validators.required]),
      mAutores: new FormControl('', [Validators.required]),
      mDisponibles: new FormControl('', [Validators.required]),
      mPrestados: new FormControl('', [Validators.required]),
      mApartados: new FormControl('', [Validators.required])
    });
  }

  limpiarFormularioNuevo() {
    this.nuevoLibroForm = new FormGroup({
      titulo: new FormControl('', [Validators.required]),
      isbn: new FormControl('', [Validators.required]),
      paginas: new FormControl('', [Validators.required]),
      existencia: new FormControl('', [Validators.required]),
      edicion: new FormControl('', [Validators.required]),
      editorial: new FormControl('', [Validators.required]),
      categorias: new FormControl('', [Validators.required]),
      autores: new FormControl('', [Validators.required]),
      disponibles: new FormControl({value: 0, disabled: true}, [Validators.required]),
      prestados: new FormControl({value: 0, disabled: true}, [Validators.required]),
      apartados: new FormControl({value: 0, disabled: true}, [Validators.required])
    });
  }
}

@Component({
  selector: 'card-detalle',
  templateUrl: 'card-detalle.html',
})
export class LibrosComponentDialog {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }
}
