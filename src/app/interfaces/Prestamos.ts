export interface Prestamo {
  idPrestamo: number
  vencido: boolean
  cuota: number
  autorizado: boolean
  fechaPrestamo: string
  fechaLimite: string
  renovaciones: number
  fechaApartado: string
  titulo: string
  isbn: string
  paginas: number
  nombre: string
  nombreAdministrador: string
  tipoUsuario: string
  matricula: string
  carrera: string
  semestre: number
  edicion: number
  editorial: string
  fechaDevolucion: string
}
