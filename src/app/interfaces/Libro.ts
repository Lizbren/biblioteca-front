export interface Libro {
  idLibro: number;
  titulo: string;
  isbn: string;
  paginas: number;
  existencia: number;
  edicion: number;
  editorial: string;
  idEditorial: number;
  categorias: Array<string>;
  idCategorias: number;
  autores: Array<string>;
  idAutores: number;
  disponibles: number;
  prestados: number;
  apartados: number;
}
