import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'biblioteca';
  nombres: string = '';
  matricula: string = '';
  permisos: string = '';
  tipoUsuario: string = '';
  panelOpenState = false;
  seccion = [
    {nombre: 'Inicio', paginas: [{pagina: 'Inicio', id: 0, ruta: 'home'}]},
    {
      nombre: 'Catalogos', paginas: [
        {pagina: 'Autores', id: 1, ruta: 'autores'},
        {pagina: 'Categorias', id: 2, ruta: 'categorias'},
        {pagina: 'Libros', id: 3, ruta: 'libros'},
        {pagina: 'Editoriales', id: 4, ruta: 'editoriales'},
        {pagina: 'Carreras', id: 5, ruta: 'carreras'},
        {pagina: 'Tipo de Usuarios', id: 13, ruta: 'tipo-usuarios'},
      ]
    },
    {nombre: 'Usuarios', paginas: [{pagina: 'Usuarios', id: 14, ruta: 'nuevo-usuario'}]},
    {
      nombre: 'Prestamos', paginas: [
        {pagina: 'Control de prestamos', id: 7, ruta: 'control-prestamos'},
        {pagina: 'Prestamos', id: 10, ruta: 'prestamos'},
        {pagina: 'Solicitar Prestamo', id: 11, ruta: 'solicitar-prestamo'}
      ]
    },
    {
      nombre: 'Usuario', paginas: [
        {pagina: 'Mi Usuario', id: 8, ruta: 'usuario'},
        {pagina: 'Mi Usuario', id: 12, ruta: 'usuario'},
        {pagina: 'Mi Super Administrador', id: 15, ruta: 'super-administrador'}
      ]
    },
  ]
  logueado: boolean = false;
  paginasPermitidas: Array<any> = [];

  constructor(readonly router: Router) {
    this.loadMenu();
  }

  loadMenu() {
    this.logueado = (sessionStorage?.logged === 'true');
    if (this.logueado) {
      this.paginasPermitidas = [];
      this.logueado = true;
      this.matricula = sessionStorage?.matricula;
      this.nombres = sessionStorage?.nombre;
      this.permisos = sessionStorage?.permisos;
      this.tipoUsuario = sessionStorage?.tipoUsuario;
      this.seccion
        .forEach(miSeccion => {
          this.permisos.split(',')
            .map(id => parseInt(id, 10))
            .forEach(id => {
              let encontrada = miSeccion.paginas.find(page => page.id === id);
              if (encontrada) {
                if (this.paginasPermitidas.length > 0) {
                  let indice = this.paginasPermitidas.findIndex(permitidas => permitidas.nombre === miSeccion.nombre);
                  if (indice !== -1) {
                    this.paginasPermitidas[indice].paginas.push(encontrada);
                  } else {
                    this.paginasPermitidas.push({nombre: miSeccion.nombre, paginas: [encontrada]})
                  }
                } else {
                  this.paginasPermitidas.push({nombre: miSeccion.nombre, paginas: [encontrada]})
                }
              }
            })
        });
      console.log(this.paginasPermitidas)
      this.tipoUsuario = sessionStorage?.tipoUsuario;
    }
  }

  closeSession() {
    sessionStorage.clear();
    this.matricula = '';
    this.nombres = '';
    this.permisos = '';
    this.tipoUsuario = '';
    this.logueado = false;
    this.paginasPermitidas = [];
    this.router.navigateByUrl('')
  }
}
